node ('SUSELinux') {

   cleanWs()

   parameters {
        string(name: 'registryUrl', defaultValue: 'niybasicregistry.azurecr.io', description: 'URL of registry to push images to')
        string(name: 'credentialsID', defaultValue: 'fe439578-9699-4e6e-b1d9-657a5756e5c9', description: 'Credentials ID of registry')
        string(name: 'registryUrl2', defaultValue: '', description: 'URL of second registry to push images to')
        string(name: 'credentialsID2', defaultValue: '', description: 'Credentials ID of second registry')
        string(name: 'reposPrefix', defaultValue: '', description: 'Prefix of repositories')
    }

    if (params.reposPrefix != null) {
        prefix = params.reposPrefix
    } else {
        prefix = ''
    }

   // Get the code from a GitHub repository
   stage('Preparation') {
      checkout scm

      // BMS files need to be built on Windows so stash the source, Ant script and settings files so they will
      // be available to the Windows build node
      dir ('Sources/bms') {stash includes: '*.bms, *.BMS', name: 'BMSStash'}
      dir ('Projects/Eclipse/BankDemo') {stash includes: '.cobolBuild', name: 'BuildFileStash'}
      dir ('Projects/Eclipse/BankDemo/.settings') {stash includes: '.bms.mfdirset', name: 'BMSSettingsFileStash'}
   }

   // Build the BMS files - BMS processor only available on Windows
   stage ('BuildBMSFiles') {
      node ('Win2019Host') {
         dir ('Sources/bms') {unstash name: 'BMSStash'}
         dir ('Projects/Eclipse/BankDemo') {unstash name: 'BuildFileStash'}
         dir ('Projects/Eclipse/BankDemo/.settings') {unstash name: 'BMSSettingsFileStash'}
         bat 'docker run --rm -w %WORKSPACE%/Projects/Eclipse/BankDemo -v %WORKSPACE%:%WORKSPACE% microfocus/edbuildtools-build:win_6.0_x64 cmd /c "c:\\EDTools\\SetupEnv.bat && ant -logger com.microfocus.ant.CommandLineLogger -f .cobolBuild bmsbuild"'
         dir ('Projects/Eclipse/BankDemo/loadlib') {stash includes: '*.MOD', name: 'BMSModStash'}
         dir ('Projects/Eclipse/BankDemo') {stash includes: '*.cpy', name: 'BMSCpyStash'}
      }
   }

   // Build the deployment image
   stage('BuildApplication') {
      dir ('Projects/Eclipse/BankDemo') {unstash name: 'BMSCpyStash'}
      dir ('Projects/Eclipse/BankDemo/loadlib') {unstash name: 'BMSModStash'}

      def dockerfile = 'System/Dockerfile'

      // Build up to running the unit tests, so we can extract the test result reports
      sh "docker build -t bankdemotests:${env.BUILD_NUMBER} -f ${dockerfile} --target RunUnitTests ."
      dir ('testresults')
      {
           sh "docker run --rm -v `pwd`:/testresults bankdemotests:${env.BUILD_NUMBER} sh -c 'cp /runtests/*.xml /testresults'"
           junit '*.xml'
           sh "docker rmi bankdemotests:${env.BUILD_NUMBER}"
      }

      // Assemble the application deployment image
      sh "docker build -t ${prefix}bankdemo:base -f ${dockerfile} ."
   }

   // Build the openldap image
   stage('AssembleOpenLDAP') {
      docker.build("${prefix}openldap:base", "-f OpenLDAP/Dockerfile ./OpenLDAP")
   }

   // Build the ES custom metrics image
   stage('AssembleMetrics') {
      docker.build("${prefix}es-metrics:base", "-f metrics/Dockerfile ./metrics")
   }

   // Publish the deployment image to the chosen registry
   stage ('PublishToRegistries') {
       publishToSingleRegistry(params.registryUrl, params.credentialsID)
       publishToSingleRegistry(params.registryUrl2, params.credentialsID2)
   }

   stage('CleanupImages') {
       // Delete previous "latest" images and remove intermediate stages
       sh "docker rmi -f \$(docker images -q --filter label=stage=intermediate)"
       sh "docker images | grep \"none\" | awk '{ print \$3 }' | xargs -r docker rmi -f"
   }

}

def publishToSingleRegistry(registryUrl, credentialsID) {
    if (registryUrl != null) {
        if (credentialsID == null) {
            docker.withRegistry("https://${registryUrl}") {
                docker.image("${prefix}bankdemo:base").push('latest')
                docker.image("${prefix}openldap:base").push('latest')
                docker.image("${prefix}es-metrics:base").push('latest')
            }
        } else {
            docker.withRegistry("https://${registryUrl}", "${credentialsID}") {
                docker.image("${prefix}bankdemo:base").push('latest')
                docker.image("${prefix}openldap:base").push('latest')
                docker.image("${prefix}es-metrics:base").push('latest')
            }
        }
    }
}