       copy "mfunit_prototypes.cpy".
       
       identification division.
       program-id. TestUDATECNV.

       environment division.
       configuration section.

       data division.
       working-storage section.
       78 TEST-TestUDATECNV-Day      value "TestUDATECNVDay".
       78 TEST-TestUDATECNV-YYYYDate value "TestUDATECNVYYYYDate".      
       78 TEST-TestUDATECNV-YYDate   value "TestUDATECNVYYDate".        
       copy "mfunit.cpy".
       
       01  WS-SYS-DATE                             PIC x(10).          
       01  WS-PRINTED.                                                
           10  WS-PRINTED-DATE.                                         
             15  FILLER                            PIC X(9)             
                 VALUE 'Printed: '.                                     
             15  WS-PRINT-DATE                     PIC X(11)            
                 VALUE 'dd mmm yyyy'.                                   
           10  WS-PRINTED-TIME.                                         
             15  FILLER                            PIC X(12)            
                 VALUE SPACES.                                          
             15  WS-PRINT-TIME.                                         
               20  WS-PRINT-TIME-HH                PIC X(2).            
               20  WS-PRINT-TIME-DOT1              PIC X(1).            
               20  WS-PRINT-TIME-MM                PIC X(2).            
               20  WS-PRINT-TIME-DOT2              PIC X(1).            
               20  WS-PRINT-TIME-SS                PIC X(2).            
       01  WS-DATE-WORK-AREA.                                           
       COPY CDATED.                                                     
      

       procedure division.

       entry MFU-TC-PREFIX & TEST-TestUDATECNV-Day.
           
           ACCEPT WS-SYS-DATE FROM DAY.
           move "10345" to WS-SYS-DATE
           SET DD-ENV-NULL TO TRUE.                                     
           SET DDI-YYDDD TO TRUE.                                       
           MOVE WS-SYS-DATE TO DDI-DATA.                                
           SET DDO-DD-MMM-YYYY TO TRUE.                                 
           CALL 'UDATECNV' USING WS-DATE-WORK-AREA.                     
           MOVE FUNCTION LOWER-CASE(DDO-DATA-DD-MMM-YYYY-MMM(2:2))      
             TO DDO-DATA-DD-MMM-YYYY-MMM(2:2).                          
           MOVE DDO-DATA TO WS-PRINT-DATE.    
           if ws-print-date = "11.Dec.2010"
               goback returning MFU-PASS-RETURN-CODE
           else
               goback returning MFU-FAIL-RETURN-CODE
           end-if
           .

       entry MFU-TC-PREFIX & TEST-TestUDATECNV-YYDate.
           
           ACCEPT WS-SYS-DATE FROM DAY.
           move "101212" to WS-SYS-DATE
           SET DD-ENV-NULL TO TRUE.                                     
           SET DDI-YYMMDD TO TRUE.                                      
           MOVE WS-SYS-DATE TO DDI-DATA.                                
           SET DDO-DD-MMM-YYYY TO TRUE.                                 
           CALL 'UDATECNV' USING WS-DATE-WORK-AREA.                     
           MOVE FUNCTION LOWER-CASE(DDO-DATA-DD-MMM-YYYY-MMM(2:2))      
             TO DDO-DATA-DD-MMM-YYYY-MMM(2:2).                          
           MOVE DDO-DATA TO WS-PRINT-DATE.    
           if ws-print-date = "12.Dec.2010"
               goback returning MFU-PASS-RETURN-CODE
           else
               goback returning MFU-FAIL-RETURN-CODE
           end-if
           .

       entry MFU-TC-PREFIX & TEST-TestUDATECNV-YYYYDate.
           
           ACCEPT WS-SYS-DATE FROM DAY.
           move "20101213" to WS-SYS-DATE
           SET DD-ENV-NULL TO TRUE.                                     
           SET DDI-YYYYMMDD TO TRUE.                                    
           MOVE WS-SYS-DATE TO DDI-DATA.                                
           SET DDO-DD-MMM-YYYY TO TRUE.                                 
           CALL 'UDATECNV' USING WS-DATE-WORK-AREA.                     
           MOVE FUNCTION LOWER-CASE(DDO-DATA-DD-MMM-YYYY-MMM(2:2))      
             TO DDO-DATA-DD-MMM-YYYY-MMM(2:2).                          
           MOVE DDO-DATA TO WS-PRINT-DATE.    
           if ws-print-date = "13.Dec.2010"
               goback returning MFU-PASS-RETURN-CODE
           else
               goback returning MFU-FAIL-RETURN-CODE
           end-if
           .

       end program.
