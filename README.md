# BankDemo
- Example CICS and JCL banking application

## Overview
- this directory contains a simple banking application consisting of CICS online and JCL COBOL programs

## To build
- Build using Jenkins by creating pipeline jobs for: Jenkinsfile

## Container port usage
- 34568 is the WS port which is used to submit JCL
- 34571 is the telnet port used for TN3270 connections
- 10086 is the ES Common Web Administration server port

## To run using docker
- Before running the first time create a volume for the application data: docker volume create bankdemo-catalog
- docker run --rm -p23:34571 -p8080:10086 -p8687:34568 --mount source=bankdemo-catalog,target=/home/esadm/deploy/catalog -eSTART_ESCWA=Y --name bankdemo niybasicregistry.azurecr.io/bankdemo:latest
- connect 3270 terminal emulator to localhost:23 and enjoy green screen !
- to view the region access ESCWA at http://localhost:8080
- to submit JCL use: cassub /stcp:localhost:8687 /j&lt;filename&gt;