#!/bin/sh
. $MFPRODBASE/bin/cobsetenv $MFPRODBASE

$COBDIR/bin/mfds64 &
while ! curl --output /dev/null --silent --fail http://127.0.0.1:$CCITCP2_PORT; do sleep 1; done;
$COBDIR/bin/mfds64 /g 5 /regionin/$1.xml S

# How to know where this directory is $ESP/RDEF??
#$COBDIR/bin/caspcrd /c /dp=$ESP/RDEF
#$COBDIR/bin/casrdtup -q -ue:dfhurdupxml -e -f/home/esadm/deploy/$2_sit.xml -v -o -op${ESP}/RDEF
#$COBDIR/bin/casrdtup -q -ue:dfhurdupxml -e -f/home/esadm/deploy/$2_stul.xml -v -o -op${ESP}/RDEF
#$COBDIR/bin/casrdtup -q -ue:dfhurdupxml -e -f/home/esadm/deploy/$2_grps.xml -v -o -op${ESP}/RDEF

$COBDIR/bin/escwa  --BasicConfig.MfRequestedEndpoint="tcp:*:10086" &
while inotifywait  -e close_write /opt/microfocus/config/34560/mfds; do $COBDIR/bin/mfds64 -x 5 /regionout $1 S ; ls -R /opt/microfocus/config/34560/secrets; done