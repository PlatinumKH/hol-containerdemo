# Secrets
- Contains the files which are encoded as scerets in Kubernetes, these would not normally be held in source control as they need to be strictly controlled and encrypted when at rest.
- for ***AZURE only***, the file needs to be patched with an alternative user name.
- run: kubectl create secret generic mfdbfh-config --from-file=MFDBFH.cfg

