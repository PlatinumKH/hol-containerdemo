000100***************************************************************** bbank20p
000200*                                                               * bbank20p
000300*   Copyright (C) 1998-2010 Micro Focus. All Rights Reserved.   * bbank20p
000400*   This demonstration program is provided for use by users     * bbank20p
000500*   of Micro Focus products and may be used, modified and       * bbank20p
000600*   distributed as part of your application provided that       * bbank20p
000700*   you properly acknowledge the copyright of Micro Focus       * bbank20p
000800*   in this material.                                           * bbank20p
000900*                                                               * bbank20p
001000***************************************************************** bbank20p
001100                                                                  bbank20p
001200***************************************************************** bbank20p
001300* Program:     BBANK20P.CBL                                     * bbank20p
001400* Layer:       Business logic                                   * bbank20p
001500* Function:    Determine users options                          * bbank20p
001600***************************************************************** bbank20p
001700                                                                  bbank20p
001800 IDENTIFICATION DIVISION.                                         bbank20p
001900 PROGRAM-ID.                                                      bbank20p
002000     BBANK20P.                                                    bbank20p
002100 DATE-WRITTEN.                                                    bbank20p
002200     September 2002.                                              bbank20p
002300 DATE-COMPILED.                                                   bbank20p
002400     Today.                                                       bbank20p
002500                                                                  bbank20p
002600 ENVIRONMENT DIVISION.                                            bbank20p
002700                                                                  bbank20p
002800 DATA DIVISION.                                                   bbank20p
002900 WORKING-STORAGE SECTION.                                         bbank20p
003000 01  WS-MISC-STORAGE.                                             bbank20p
003100   05  WS-PROGRAM-ID                         PIC X(8)             bbank20p
003200       VALUE 'BBANK20P'.                                          bbank20p
003300   05  WS-INPUT-FLAG                         PIC X(1).            bbank20p
003400     88  INPUT-OK                            VALUE '0'.           bbank20p
003500     88  INPUT-ERROR                         VALUE '1'.           bbank20p
003600   05  WS-RETURN-FLAG                        PIC X(1).            bbank20p
003700     88  WS-RETURN-FLAG-OFF                  VALUE LOW-VALUES.    bbank20p
003800     88  WS-RETURN-FLAG-ON                   VALUE '1'.           bbank20p
003900   05  WS-RETURN-MSG                         PIC X(75).           bbank20p
004000     88  WS-RETURN-MSG-OFF                   VALUE SPACES.        bbank20p
004100   05  WS-PFK-FLAG                           PIC X(1).            bbank20p
004200     88  PFK-VALID                           VALUE '0'.           bbank20p
004300     88  PFK-INVALID                         VALUE '1'.           bbank20p
004400   05  WS-ERROR-MSG                          PIC X(75).           bbank20p
004500   05  WS-SUB1                               PIC S9(4) COMP.      bbank20p
004600   05  WS-SUB1-LIMIT                         PIC S9(4) COMP.      bbank20p
004700   05  WS-SEL-COUNT                          PIC 9(1).            bbank20p
004800   05  WS-SEL-OPTION                         PIC X(1).            bbank20p
004900     88  WS-SEL-OPTION-NULL                  VALUES ' ', '.'.     bbank20p
005000     88  WS-SEL-OPTION-DISPLAY               VALUE 'D'.           bbank20p
005100     88  WS-SEL-OPTION-TRANSFER              VALUE 'X'.           bbank20p
005200     88  WS-SEL-OPTION-UPDATE                VALUE 'U'.           bbank20p
005300     88  WS-SEL-OPTION-LOAN                  VALUE 'L'.           bbank20p
005400     88  WS-SEL-OPTION-PRINT                 VALUE 'P'.           bbank20p
005500     88  WS-SEL-OPTION-INFO                  VALUE 'I'.           bbank20p
005600     88  WS-SEL-OPTION-ERRORS                VALUE 'Z'.           bbank20p
005700   05  WS-SEL-MATRIX                         PIC X(7).            bbank20p
005800                                                                  bbank20p
005900 01  WS-BANK-DATA.                                                bbank20p
006000 COPY CBANKDAT.                                                   bbank20p
006100                                                                  bbank20p
006200 01  WS-HELP-DATA.                                                bbank20p
006300 COPY CHELPD01.                                                   bbank20p
006400                                                                  bbank20p
006500 01  WS-ACCOUNT-DATA.                                             bbank20p
006600 COPY CBANKD08.                                                   bbank20p
006700                                                                  bbank20p
006800 COPY CABENDD.                                                    bbank20p
006900                                                                  bbank20p
007000 LINKAGE SECTION.                                                 bbank20p
007100 01  DFHCOMMAREA.                                                 bbank20p
007200   05  LK-COMMAREA                           PIC X(6144).         bbank20p
007300                                                                  bbank20p
007400 COPY CENTRY.                                                     bbank20p
007500***************************************************************** bbank20p
007600* Make ourselves re-entrant                                     * bbank20p
007700***************************************************************** bbank20p
007800     MOVE SPACES TO WS-ERROR-MSG.                                 bbank20p
007900                                                                  bbank20p
008000***************************************************************** bbank20p
008100* Move the passed area to our area                              * bbank20p
008200***************************************************************** bbank20p
008300     MOVE DFHCOMMAREA (1:LENGTH OF WS-BANK-DATA) TO WS-BANK-DATA. bbank20p
008400                                                                  bbank20p
008500***************************************************************** bbank20p
008600* Ensure error message is cleared                               * bbank20p
008700***************************************************************** bbank20p
008800     MOVE SPACES TO BANK-ERROR-MSG.                               bbank20p
008900                                                                  bbank20p
009000***************************************************************** bbank20p
009100* This is the main process                                      * bbank20p
009200***************************************************************** bbank20p
009300                                                                  bbank20p
009400***************************************************************** bbank20p
009500* Initialize the list of options available                      * bbank20p
009600***************************************************************** bbank20p
009700     MOVE SPACES TO WS-SEL-MATRIX.                                bbank20p
009800                                                                  bbank20p
009900***************************************************************** bbank20p
010000* Save the passed return message and then turn it off           * bbank20p
010100***************************************************************** bbank20p
010200     MOVE BANK-RETURN-MSG TO WS-RETURN-MSG.                       bbank20p
010300     SET BANK-RETURN-MSG-OFF TO TRUE.                             bbank20p
010400                                                                  bbank20p
010500     MOVE WS-RETURN-MSG TO WS-ERROR-MSG.                          bbank20p
010600                                                                  bbank20p
010700***************************************************************** bbank20p
010800* Check the AID to see if its valid at this point               * bbank20p
010900***************************************************************** bbank20p
011000     SET PFK-INVALID TO TRUE.                                     bbank20p
011100     IF BANK-AID-ENTER OR                                         bbank20p
011200        BANK-AID-PFK03 OR                                         bbank20p
011300        BANK-AID-PFK04                                            bbank20p
011400        SET PFK-VALID TO TRUE                                     bbank20p
011500     END-IF.                                                      bbank20p
011600     IF BANK-AID-PFK01 AND                                        bbank20p
011700        BANK-HELP-INACTIVE                                        bbank20p
011800        SET BANK-HELP-ACTIVE TO TRUE                              bbank20p
011900        SET PFK-VALID TO TRUE                                     bbank20p
012000     END-IF.                                                      bbank20p
012100     IF PFK-INVALID                                               bbank20p
012200        SET BANK-AID-ENTER TO TRUE                                bbank20p
012300     END-IF.                                                      bbank20p
012400                                                                  bbank20p
012500***************************************************************** bbank20p
012600* Check the AID to see if we have to quit                       * bbank20p
012700***************************************************************** bbank20p
012800     IF BANK-AID-PFK03                                            bbank20p
012900        MOVE 'BBANK20P' TO BANK-LAST-PROG                         bbank20p
013000        MOVE 'BBANK99P' TO BANK-NEXT-PROG                         bbank20p
013100        MOVE 'MBANK99' TO BANK-NEXT-MAPSET                        bbank20p
013200        MOVE 'BANK99A' TO BANK-NEXT-MAP                           bbank20p
013300        GO TO COMMON-RETURN                                       bbank20p
013400     END-IF.                                                      bbank20p
013500                                                                  bbank20p
013600***************************************************************** bbank20p
013700* Check the to see if user needs or has been using help         * bbank20p
013800***************************************************************** bbank20p
013900     IF BANK-HELP-ACTIVE                                          bbank20p
014000        IF BANK-AID-PFK04                                         bbank20p
014100           SET BANK-HELP-INACTIVE TO TRUE                         bbank20p
014200           MOVE 00 TO BANK-HELP-SCREEN                            bbank20p
014300           MOVE 'BBANK20P' TO BANK-LAST-PROG                      bbank20p
014400           MOVE 'BBANK20P' TO BANK-NEXT-PROG                      bbank20p
014500           MOVE 'MBANK20' TO BANK-LAST-MAPSET                     bbank20p
014600           MOVE 'HELP20A' TO BANK-LAST-MAP                        bbank20p
014700           MOVE 'MBANK20' TO BANK-NEXT-MAPSET                     bbank20p
014800           MOVE 'BANK20A' TO BANK-NEXT-MAP                        bbank20p
014900           PERFORM POPULATE-OPTIONS THRU                          bbank20p
015000                   POPULATE-OPTIONS-EXIT                          bbank20p
015100           GO TO COMMON-RETURN                                    bbank20p
015200        ELSE                                                      bbank20p
015300           MOVE 01 TO BANK-HELP-SCREEN                            bbank20p
015400           MOVE 'BBANK20P' TO BANK-LAST-PROG                      bbank20p
015500           MOVE 'BBANK20P' TO BANK-NEXT-PROG                      bbank20p
015600           MOVE 'MBANK20' TO BANK-LAST-MAPSET                     bbank20p
015700           MOVE 'BANK20A' TO BANK-LAST-MAP                        bbank20p
015800           MOVE 'MBANK20' TO BANK-NEXT-MAPSET                     bbank20p
015900           MOVE 'HELP20A' TO BANK-NEXT-MAP                        bbank20p
016000           MOVE 'BANK20' TO HELP01I-SCRN                          bbank20p
016100           COPY CHELPX01.                                         bbank20p
016200           MOVE HELP01O-DATA TO BANK-HELP-DATA                    bbank20p
016300           GO TO COMMON-RETURN                                    bbank20p
016400     END-IF.                                                      bbank20p
016500                                                                  bbank20p
016600***************************************************************** bbank20p
016700* Check the AID to see if we have to return to previous screen  * bbank20p
016800***************************************************************** bbank20p
016900     IF BANK-AID-PFK04                                            bbank20p
017000        MOVE 'BBANK20P' TO BANK-LAST-PROG                         bbank20p
017100        MOVE 'BBANK10P' TO BANK-NEXT-PROG                         bbank20p
017200        MOVE 'MBANK10' TO BANK-NEXT-MAPSET                        bbank20p
017300        MOVE 'BANK10A' TO BANK-NEXT-MAP                           bbank20p
017400        SET BANK-AID-ENTER TO TRUE                                bbank20p
017500        SET BANK-NO-CONV-IN-PROGRESS TO TRUE                      bbank20p
017600        GO TO COMMON-RETURN                                       bbank20p
017700     END-IF.                                                      bbank20p
017800                                                                  bbank20p
017900* Check if we have set the screen up before or is this 1st time   bbank20p
018000     IF BANK-LAST-MAPSET IS NOT EQUAL TO 'MBANK20'                bbank20p
018100        MOVE WS-RETURN-MSG TO BANK-ERROR-MSG                      bbank20p
018200        MOVE 'BBANK20P' TO BANK-LAST-PROG                         bbank20p
018300        MOVE 'BBANK20P' TO BANK-NEXT-PROG                         bbank20p
018400        MOVE 'MBANK20' TO BANK-LAST-MAPSET                        bbank20p
018500        MOVE 'BANK20A' TO BANK-LAST-MAP                           bbank20p
018600        MOVE 'MBANK20' TO BANK-NEXT-MAPSET                        bbank20p
018700        MOVE 'BANK20A' TO BANK-NEXT-MAP                           bbank20p
018800        MOVE LOW-VALUES TO BANK-SCR20-SEL1IP                      bbank20p
018900        MOVE LOW-VALUES TO BANK-SCR20-SEL2IP                      bbank20p
019000        MOVE LOW-VALUES TO BANK-SCR20-SEL3IP                      bbank20p
019100        MOVE LOW-VALUES TO BANK-SCR20-SEL4IP                      bbank20p
019200        MOVE LOW-VALUES TO BANK-SCR20-SEL5IP                      bbank20p
019300        MOVE LOW-VALUES TO BANK-SCR20-SEL6IP                      bbank20p
019400        MOVE LOW-VALUES TO BANK-SCR20-SEL7IP                      bbank20p
019500        IF GUEST                                                  bbank20p
019600           MOVE 'LI     ' TO WS-SEL-MATRIX                        bbank20p
019700        ELSE                                                      bbank20p
019800          MOVE SPACES TO CD08-DATA                                bbank20p
019900          MOVE BANK-USERID TO CD08I-CONTACT-ID                    bbank20p
020000* Now go get the data                                             bbank20p
020100          COPY CBANKX08.                                          bbank20p
020200          IF CD08O-COUNT IS EQUAL TO 0                            bbank20p
020300             IF PROBLEM-USER                                      bbank20p
020400                MOVE 'LIZ    ' TO WS-SEL-MATRIX                   bbank20p
020500             ELSE                                                 bbank20p
020600                MOVE 'LI     ' TO WS-SEL-MATRIX                   bbank20p
020700             END-IF                                               bbank20p
020800          END-IF                                                  bbank20p
020900          IF CD08O-COUNT IS EQUAL TO 1                            bbank20p
021000             IF PROBLEM-USER                                      bbank20p
021100                MOVE 'DULPIZ ' TO WS-SEL-MATRIX                   bbank20p
021200             ELSE                                                 bbank20p
021300                MOVE 'DULPI  ' TO WS-SEL-MATRIX                   bbank20p
021400             END-IF                                               bbank20p
021500          END-IF                                                  bbank20p
021600          IF CD08O-COUNT IS GREATER THAN 1                        bbank20p
021700             IF PROBLEM-USER                                      bbank20p
021800                MOVE 'DXULPIZ' TO WS-SEL-MATRIX                   bbank20p
021900             ELSE                                                 bbank20p
022000                MOVE 'DXULPI ' TO WS-SEL-MATRIX                   bbank20p
022100             END-IF                                               bbank20p
022200          END-IF                                                  bbank20p
022300        END-IF                                                    bbank20p
022400        PERFORM POPULATE-OPTIONS THRU                             bbank20p
022500                POPULATE-OPTIONS-EXIT                             bbank20p
022600        GO TO COMMON-RETURN                                       bbank20p
022700     END-IF.                                                      bbank20p
022800                                                                  bbank20p
022900     PERFORM VALIDATE-DATA THRU                                   bbank20p
023000             VALIDATE-DATA-EXIT.                                  bbank20p
023100                                                                  bbank20p
023200* If we had an error display error and return                     bbank20p
023300     IF INPUT-ERROR                                               bbank20p
023400        MOVE WS-ERROR-MSG TO BANK-ERROR-MSG                       bbank20p
023500        MOVE 'BBANK20P' TO BANK-LAST-PROG                         bbank20p
023600        MOVE 'BBANK20P' TO BANK-NEXT-PROG                         bbank20p
023700        MOVE 'MBANK20' TO BANK-LAST-MAPSET                        bbank20p
023800        MOVE 'BANK20A' TO BANK-LAST-MAP                           bbank20p
023900        MOVE 'MBANK20' TO BANK-NEXT-MAPSET                        bbank20p
024000        MOVE 'BANK20A' TO BANK-NEXT-MAP                           bbank20p
024100        PERFORM POPULATE-OPTIONS THRU                             bbank20p
024200                POPULATE-OPTIONS-EXIT                             bbank20p
024300        GO TO COMMON-RETURN                                       bbank20p
024400     END-IF.                                                      bbank20p
024500                                                                  bbank20p
024600     IF BANK-SCR20-SEL1IP IS NOT EQUAL TO LOW-VALUES              bbank20p
024700        MOVE BANK-SCR20-SEL1ID TO WS-SEL-OPTION                   bbank20p
024800     END-IF.                                                      bbank20p
024900     IF BANK-SCR20-SEL2IP IS NOT EQUAL TO LOW-VALUES              bbank20p
025000        MOVE BANK-SCR20-SEL2ID TO WS-SEL-OPTION                   bbank20p
025100     END-IF.                                                      bbank20p
025200     IF BANK-SCR20-SEL3IP IS NOT EQUAL TO LOW-VALUES              bbank20p
025300        MOVE BANK-SCR20-SEL3ID TO WS-SEL-OPTION                   bbank20p
025400     END-IF.                                                      bbank20p
025500     IF BANK-SCR20-SEL4IP IS NOT EQUAL TO LOW-VALUES              bbank20p
025600        MOVE BANK-SCR20-SEL4ID TO WS-SEL-OPTION                   bbank20p
025700     END-IF.                                                      bbank20p
025800     IF BANK-SCR20-SEL5IP IS NOT EQUAL TO LOW-VALUES              bbank20p
025900        MOVE BANK-SCR20-SEL5ID TO WS-SEL-OPTION                   bbank20p
026000     END-IF.                                                      bbank20p
026100     IF BANK-SCR20-SEL6IP IS NOT EQUAL TO LOW-VALUES              bbank20p
026200        MOVE BANK-SCR20-SEL6ID TO WS-SEL-OPTION                   bbank20p
026300     END-IF.                                                      bbank20p
026400     IF BANK-SCR20-SEL7IP IS NOT EQUAL TO LOW-VALUES              bbank20p
026500        MOVE BANK-SCR20-SEL7ID TO WS-SEL-OPTION                   bbank20p
026600     END-IF.                                                      bbank20p
026700                                                                  bbank20p
026800     IF WS-SEL-OPTION IS EQUAL TO 'D'                             bbank20p
026900        MOVE 'BBANK30P' TO BANK-NEXT-PROG                         bbank20p
027000        GO TO COMMON-RETURN                                       bbank20p
027100     END-IF.                                                      bbank20p
027200                                                                  bbank20p
027300     IF WS-SEL-OPTION IS EQUAL TO 'X'                             bbank20p
027400        MOVE 'BBANK50P' TO BANK-NEXT-PROG                         bbank20p
027500        GO TO COMMON-RETURN                                       bbank20p
027600     END-IF.                                                      bbank20p
027700                                                                  bbank20p
027800     IF WS-SEL-OPTION IS EQUAL TO 'U'                             bbank20p
027900        MOVE 'BBANK60P' TO BANK-NEXT-PROG                         bbank20p
028000        MOVE SPACES TO BANK-SCREEN60-DATA                         bbank20p
028100        MOVE BANK-USERID TO BANK-SCR60-CONTACT-ID                 bbank20p
028200        MOVE BANK-USERID-NAME TO BANK-SCR60-CONTACT-NAME          bbank20p
028300        SET ADDR-CHANGE-REQUEST TO TRUE                           bbank20p
028400        GO TO COMMON-RETURN                                       bbank20p
028500     END-IF.                                                      bbank20p
028600                                                                  bbank20p
028700     IF WS-SEL-OPTION IS EQUAL TO 'L'                             bbank20p
028800        MOVE 'BBANK70P' TO BANK-NEXT-PROG                         bbank20p
028900        GO TO COMMON-RETURN                                       bbank20p
029000     END-IF.                                                      bbank20p
029100                                                                  bbank20p
029200     IF WS-SEL-OPTION IS EQUAL TO 'P'                             bbank20p
029300        MOVE 'BBANK80P' TO BANK-NEXT-PROG                         bbank20p
029400        GO TO COMMON-RETURN                                       bbank20p
029500     END-IF.                                                      bbank20p
029600                                                                  bbank20p
029700     IF WS-SEL-OPTION IS EQUAL TO 'I'                             bbank20p
029800        MOVE 'BBANK90P' TO BANK-NEXT-PROG                         bbank20p
029900        GO TO COMMON-RETURN                                       bbank20p
030000     END-IF.                                                      bbank20p
030100                                                                  bbank20p
030200     IF WS-SEL-OPTION IS EQUAL TO 'Z'                             bbank20p
030300        MOVE 'BBANKZZP' TO BANK-NEXT-PROG                         bbank20p
030400        GO TO COMMON-RETURN                                       bbank20p
030500     END-IF.                                                      bbank20p
030600                                                                  bbank20p
030700***************************************************************** bbank20p
030800* If we get this far then we have an error in our logic as we   * bbank20p
030900* don't know where to go next.                                  * bbank20p
031000***************************************************************** bbank20p
031100     IF BANK-ENV-CICS                                             bbank20p
031200        MOVE WS-PROGRAM-ID TO ABEND-CULPRIT                       bbank20p
031300        MOVE '0001' TO ABEND-CODE                                 bbank20p
031400        MOVE SPACES TO ABEND-REASON                               bbank20p
031500        COPY CABENDPO.                                            bbank20p
031600     END-IF.                                                      bbank20p
031700     GOBACK.                                                      bbank20p
031800                                                                  bbank20p
031900 COMMON-RETURN.                                                   bbank20p
032000     MOVE WS-BANK-DATA TO DFHCOMMAREA (1:LENGTH OF WS-BANK-DATA). bbank20p
032100 COPY CRETURN.                                                    bbank20p
032200                                                                  bbank20p
032300 VALIDATE-DATA.                                                   bbank20p
032400     SET INPUT-OK TO TRUE.                                        bbank20p
032500     MOVE ZERO TO WS-SEL-COUNT.                                   bbank20p
032600                                                                  bbank20p
032700     IF BANK-SCR20-SEL1IP IS NOT EQUAL TO LOW-VALUES              bbank20p
032800        ADD 1 TO WS-SEL-COUNT                                     bbank20p
032900     END-IF.                                                      bbank20p
033000     IF BANK-SCR20-SEL2IP IS NOT EQUAL TO LOW-VALUES              bbank20p
033100        ADD 1 TO WS-SEL-COUNT                                     bbank20p
033200     END-IF.                                                      bbank20p
033300     IF BANK-SCR20-SEL3IP IS NOT EQUAL TO LOW-VALUES              bbank20p
033400        ADD 1 TO WS-SEL-COUNT                                     bbank20p
033500     END-IF.                                                      bbank20p
033600     IF BANK-SCR20-SEL4IP IS NOT EQUAL TO LOW-VALUES              bbank20p
033700        ADD 1 TO WS-SEL-COUNT                                     bbank20p
033800     END-IF.                                                      bbank20p
033900     IF BANK-SCR20-SEL5IP IS NOT EQUAL TO LOW-VALUES              bbank20p
034000        ADD 1 TO WS-SEL-COUNT                                     bbank20p
034100     END-IF.                                                      bbank20p
034200     IF BANK-SCR20-SEL6IP IS NOT EQUAL TO LOW-VALUES              bbank20p
034300        ADD 1 TO WS-SEL-COUNT                                     bbank20p
034400     END-IF.                                                      bbank20p
034500     IF BANK-SCR20-SEL7IP IS NOT EQUAL TO LOW-VALUES              bbank20p
034600        ADD 1 TO WS-SEL-COUNT                                     bbank20p
034700     END-IF.                                                      bbank20p
034800                                                                  bbank20p
034900                                                                  bbank20p
035000     IF WS-SEL-COUNT IS EQUAL TO ZERO                             bbank20p
035100        MOVE 'Please select an option' TO WS-ERROR-MSG            bbank20p
035200        GO TO VALIDATE-DATA-ERROR                                 bbank20p
035300     END-IF.                                                      bbank20p
035400                                                                  bbank20p
035500     IF WS-SEL-COUNT IS GREATER THAN 1                            bbank20p
035600        MOVE 'Please select a single option' TO WS-ERROR-MSG      bbank20p
035700        GO TO VALIDATE-DATA-ERROR                                 bbank20p
035800     END-IF.                                                      bbank20p
035900                                                                  bbank20p
036000     GO TO VALIDATE-DATA-EXIT.                                    bbank20p
036100 VALIDATE-DATA-ERROR.                                             bbank20p
036200     SET INPUT-ERROR TO TRUE.                                     bbank20p
036300 VALIDATE-DATA-EXIT.                                              bbank20p
036400     EXIT.                                                        bbank20p
036500                                                                  bbank20p
036600 POPULATE-OPTIONS.                                                bbank20p
036700     MOVE 0 TO WS-SUB1.                                           bbank20p
036800     DIVIDE LENGTH BANK-SCREEN20-FIELD                            bbank20p
036900       INTO LENGTH OF BANK-SCREEN20-DATA-R                        bbank20p
037000         GIVING WS-SUB1-LIMIT.                                    bbank20p
037100 POPULATE-OPTIONS-LOOP.                                           bbank20p
037200     ADD 1 TO WS-SUB1.                                            bbank20p
037300     IF WS-SUB1 IS GREATER THAN WS-SUB1-LIMIT                     bbank20p
037400        GO TO POPULATE-OPTIONS-EXIT                               bbank20p
037500     END-IF.                                                      bbank20p
037600     IF WS-SEL-MATRIX IS NOT EQUAL TO SPACES                      bbank20p
037700        MOVE WS-SEL-MATRIX (WS-SUB1:1) TO BANK-SCR20-ID (WS-SUB1) bbank20p
037800     END-IF.                                                      bbank20p
037900     MOVE SPACES TO BANK-SCR20-TX (WS-SUB1).                      bbank20p
038000     IF BANK-SCR20-ID (WS-SUB1) IS EQUAL TO 'D'                   bbank20p
038100        MOVE 'Display your account balances'                      bbank20p
038200          TO BANK-SCR20-TX (WS-SUB1)                              bbank20p
038300     END-IF.                                                      bbank20p
038400     IF BANK-SCR20-ID (WS-SUB1) IS EQUAL TO 'X'                   bbank20p
038500        MOVE 'Transfer funds between accounts'                    bbank20p
038600          TO BANK-SCR20-TX (WS-SUB1)                              bbank20p
038700     END-IF.                                                      bbank20p
038800     IF BANK-SCR20-ID (WS-SUB1) IS EQUAL TO 'U'                   bbank20p
038900        MOVE 'Update your contact information'                    bbank20p
039000          TO BANK-SCR20-TX (WS-SUB1)                              bbank20p
039100     END-IF.                                                      bbank20p
039200     IF BANK-SCR20-ID (WS-SUB1) IS EQUAL TO 'L'                   bbank20p
039300        MOVE 'Calculate the cost of a loan'                       bbank20p
039400          TO BANK-SCR20-TX (WS-SUB1)                              bbank20p
039500     END-IF.                                                      bbank20p
039600     IF BANK-SCR20-ID (WS-SUB1) IS EQUAL TO 'P'                   bbank20p
039700        MOVE 'Request printed statement(s)'                       bbank20p
039800          TO BANK-SCR20-TX (WS-SUB1)                              bbank20p
039900     END-IF.                                                      bbank20p
040000     IF BANK-SCR20-ID (WS-SUB1) IS EQUAL TO 'I'                   bbank20p
040100        MOVE 'Obtain more information'                            bbank20p
040200          TO BANK-SCR20-TX (WS-SUB1)                              bbank20p
040300     END-IF.                                                      bbank20p
040400     IF BANK-SCR20-ID (WS-SUB1) IS EQUAL TO 'Z'                   bbank20p
040500        MOVE 'Generate problems / errors'                         bbank20p
040600          TO BANK-SCR20-TX (WS-SUB1)                              bbank20p
040700     END-IF.                                                      bbank20p
040800     GO TO POPULATE-OPTIONS-LOOP.                                 bbank20p
040900 POPULATE-OPTIONS-EXIT.                                           bbank20p
041000     EXIT.                                                        bbank20p
041100                                                                  bbank20p
041200* $ Version 5.99c sequenced on Wednesday 3 Mar 2011 at 1:00pm     bbank20p
