000100*$set dialect(MF) cicsecm()                                       dfhuains
000200***************************************************************** dfhuains
000300*                                                               * dfhuains
000400*   Copyright (C) 1998-2010 Micro Focus. All Rights Reserved.   * dfhuains
000500*   This demonstration program is provided for use by users     * dfhuains
000600*   of Micro Focus products and may be used, modified and       * dfhuains
000700*   distributed as part of your application provided that       * dfhuains
000800*   you properly acknowledge the copyright of Micro Focus       * dfhuains
000900*   in this material.                                           * dfhuains
001000*                                                               * dfhuains
001100***************************************************************** dfhuains
001200                                                                  dfhuains
001300***************************************************************** dfhuains
001400* Program:     DFHUAINS.CBL                                     * dfhuains
001500* Function:    Assign terminal ids                              * dfhuains
001600***************************************************************** dfhuains
001700                                                                  dfhuains
001800 identification division.                                         dfhuains
001900 program-id.                                                      dfhuains
002000     DFHUAINS.                                                    dfhuains
002100 date-written.                                                    dfhuains
002200     September 2002.                                              dfhuains
002300 date-compiled.                                                   dfhuains
002400     Today.                                                       dfhuains
002500                                                                  dfhuains
002600 environment division.                                            dfhuains
002700                                                                  dfhuains
002800 data division.                                                   dfhuains
002900 working-storage section.                                         dfhuains
003000 01  ws-misc-storage.                                             dfhuains
003100   05  ws-program-id                         pic x(8)             dfhuains
003200       value 'DFHUAINS'.                                          dfhuains
003300   05  ws-sub                                pic s9(4) comp-5     dfhuains
003400       value 0.                                                   dfhuains
003500   05  ws-cwa-area-ptr                       pointer.             dfhuains
003600   05  ws-term-id.                                                dfhuains
003700     10  ws-term-id-alpha                    pic x(1).            dfhuains
003800     10  ws-term-id-numeric                  pic 9(3).            dfhuains
003900   05  ws-alpha.                                                  dfhuains
004000     10  ws-alpha-string                     pic x(26)            dfhuains
004100         value 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.                      dfhuains
004200     10  filler redefines ws-alpha-string.                        dfhuains
004300       15  ws-alpha-char                     pic x(1)             dfhuains
004400           occurs 26 times.                                       dfhuains
004500   05  ws-numeric.                                                dfhuains
004600     10  ws-numeric-string                   pic x(10)            dfhuains
004700         value '0123456789'.                                      dfhuains
004800     10  filler redefines ws-numeric-string.                      dfhuains
004900       15  ws-numeric-char                   pic x(1)             dfhuains
005000           occurs 10 times.                                       dfhuains
005100   05  two-bytes.                                                 dfhuains
005200     10  two-bytes-left                      pic x(1).            dfhuains
005300     10  two-bytes-right                     pic x(1).            dfhuains
005400   05  two-bytes-binary redefines two-bytes  pic 9(4) comp.       dfhuains
005500   05  ws-temp4                              pic x(4).            dfhuains
005600   05  ws-abstime                            pic s9(15) comp-3.   dfhuains
005700   05  ws-date-yyyy-mm-dd                    pic x(10).           dfhuains
005800   05  filler redefines ws-date-yyyy-mm-dd.                       dfhuains
005900     10  ws-date-yyyy                        pic x(4).            dfhuains
006000     10  ws-date-mm                          pic 9(2).            dfhuains
006100     10  ws-date-dd                          pic x(2).            dfhuains
006200   05  ws-months                             pic x(36)            dfhuains
006300       value 'JanFebMarAprMayJunJulAugSepOctNovDec'.              dfhuains
006400   05  filler redefines ws-months.                                dfhuains
006500     10  ws-month                            pic x(3)             dfhuains
006600         occurs 12 times.                                         dfhuains
006700                                                                  dfhuains
006800 01  ws-client-data.                                              dfhuains
006900   05  ws-client-port                        pic z9(5).           dfhuains
007000   05  ws-client-ip-address                  pic x(15).           dfhuains
007100   05  ws-client-ip-address-v4.                                   dfhuains
007200     10 ws-client-addr-part1                 pic zzz9.            dfhuains
007300     10 ws-client-addr-part2                 pic zzz9.            dfhuains
007400     10 ws-client-addr-part3                 pic zzz9.            dfhuains
007500     10 ws-client-addr-part4                 pic zzz9.            dfhuains
007600                                                                  dfhuains
007700 01  ws-server-data.                                              dfhuains
007800   05  ws-server-port                        pic z9(5).           dfhuains
007900   05  ws-server-ip-address                  pic x(15).           dfhuains
008000   05  ws-server-ip-address-v4.                                   dfhuains
008100     10 ws-server-addr-part1                 pic zzz9.            dfhuains
008200     10 ws-server-addr-part2                 pic zzz9.            dfhuains
008300     10 ws-server-addr-part3                 pic zzz9.            dfhuains
008400     10 ws-server-addr-part4                 pic zzz9.            dfhuains
008500                                                                  dfhuains
008600 78  78-srv-cmsg-entry-name          value 'SRV_CONSOLE_MSG'.     dfhuains
008700                                                                  dfhuains
008800 01  ws-srv-console-msg.                                          dfhuains
008900   03  value 0                               pic x(4) comp-5.     dfhuains
009000   03  value 0                               pic x(4) comp-5.     dfhuains
009100   03                                        pic x(4) comp-5.     dfhuains
009200   03  ws-srv-cmsg-size                      pic x(4) comp-5.     dfhuains
009300   03  value 0                               pic x(4) comp-5.     dfhuains
009400   03                                        pic x(4) comp-5.     dfhuains
009500   03  occurs 2                              pic x(4) comp-5.     dfhuains
009600   03  ws-srv-cmsg-ptr                       pointer.             dfhuains
009700   03                                        pointer.             dfhuains
009800   03  occurs 3                              pointer.             dfhuains
009900                                                                  dfhuains
010000 01  ws-msg                                  pic x(120)           dfhuains
010100     value z'Hello world'.                                        dfhuains
010200                                                                  dfhuains
010300                                                                  dfhuains
010400 linkage section.                                                 dfhuains
010500 01  dfhcommarea.                                                 dfhuains
010600   05  a-string                              pic x(4).            dfhuains
010700   05  data-ptr-1                            pointer.             dfhuains
010800   05  data-ptr-2                            pointer.             dfhuains
010900   05  data-ptr-3                            pointer.             dfhuains
011000   05  data-ptr-4                            pointer.             dfhuains
011100   05  data-ptr-5                            pointer.             dfhuains
011200   05  data-ptr-6                            pointer.             dfhuains
011300                                                                  dfhuains
011400 01  netname-info.                                                dfhuains
011500   05  netname-length                        pic 9(2) comp-x.     dfhuains
011600   05  netname                               pic x(8).            dfhuains
011700                                                                  dfhuains
011800 01  model-table.                                                 dfhuains
011900   05  model-table-length                    pic 9(2) comp-x.     dfhuains
012000   05  model-table-names                                          dfhuains
012100         occurs 1 to 50 times depending on model-table-length.    dfhuains
012200     10  model-table-name                    pic x(8).            dfhuains
012300                                                                  dfhuains
012400 01  tct-info.                                                    dfhuains
012500   05  tct-info-name                         pic x(8).            dfhuains
012600   05  tct-info-term-id                      pic x(4).            dfhuains
012700   05  tct-info-prr-id                       pic x(4).            dfhuains
012800   05  tct-info-alt-prt-id                   pic x(4).            dfhuains
012900   05  tct-info-status                       pic x(1).            dfhuains
013000                                                                  dfhuains
013100* Attributes of terminal user sitting at                          dfhuains
013200* may be used to match against predefined naming convention in    dfhuains
013300* autoint-name determined by installation to decide which model   dfhuains
013400* to use.                                                         dfhuains
013500 01  term-info.                                                   dfhuains
013600   05  term-info-length                      pic 9(2) comp-x.     dfhuains
013700   05  term-info-rows                        pic x(1) comp-x.     dfhuains
013800   05  term-info-cols                        pic x(1) comp-x.     dfhuains
013900   05  uctran-setting                        pic x(1).            dfhuains
014000                                                                  dfhuains
014100 01  tn3270-client-ip.                                            dfhuains
014200   05  client-ip-type                        pic x(1).            dfhuains
014300     88  client-ip-type-4-88                 value '4'.           dfhuains
014400     88  client-ip-type-6-88                 value '6'.           dfhuains
014500   05                                        pic x(1).            dfhuains
014600   05  client-ip-port                        pic x(2) comp-x.     dfhuains
014700   05  client-ip-address-6.                                       dfhuains
014800     10  client-ip-address-4                 pic x(4).            dfhuains
014900     10                                      pic x(12).           dfhuains
015000                                                                  dfhuains
015100 01  tn3270-server-ip.                                            dfhuains
015200   05  server-ip-type                        pic x(1).            dfhuains
015300     88  server-ip-type-4-88                 value '4'.           dfhuains
015400     88  server-ip-type-6-88                 value '6'.           dfhuains
015500   05                                        pic x(1).            dfhuains
015600   05  server-ip-port                        pic x(2) comp-x.     dfhuains
015700   05  server-ip-address-6.                                       dfhuains
015800     10  server-ip-address-4                 pic x(4).            dfhuains
015900     10                                      pic x(12).           dfhuains
016000                                                                  dfhuains
016100 01  cwa-area.                                                    dfhuains
016200   05  cwa-last-term-id                      pic x(4).            dfhuains
016300                                                                  dfhuains
016400 01  lk-ip-address                           pic x(15).           dfhuains
016500 01  lk-ip-address-v4x.                                           dfhuains
016600     10 lk-addr-part1x                       pic x(4).            dfhuains
016700     10 lk-addr-part2x                       pic x(4).            dfhuains
016800     10 lk-addr-part3x                       pic x(4).            dfhuains
016900     10 lk-addr-part4x                       pic x(4).            dfhuains
017000                                                                  dfhuains
017100                                                                  dfhuains
017200                                                                  dfhuains
017300 procedure division.                                              dfhuains
017400*      call 'CBL_DEBUGBREAK'.                                     dfhuains
017500     exec cics address cwa(ws-cwa-area-ptr)                       dfhuains
017600     end-exec.                                                    dfhuains
017700                                                                  dfhuains
017800     set address of cwa-area to ws-cwa-area-ptr.                  dfhuains
017900                                                                  dfhuains
018000     exec cics enq resource(cwa-area)                             dfhuains
018100     end-exec.                                                    dfhuains
018200                                                                  dfhuains
018300     set address of netname-info to data-ptr-1.                   dfhuains
018400     set address of model-table to data-ptr-2.                    dfhuains
018500     set address of tct-info to data-ptr-3.                       dfhuains
018600     set address of term-info to data-ptr-4.                      dfhuains
018700     set address of tn3270-server-ip to data-ptr-5.               dfhuains
018800     set address of tn3270-client-ip to data-ptr-6.               dfhuains
018900                                                                  dfhuains
019000*  check if we have a ip-address to work with                     dfhuains
019100*  for the connected client                                       dfhuains
019200                                                                  dfhuains
019300     if address of tn3270-client-ip not = null                    dfhuains
019400        move client-ip-port  to ws-client-port                    dfhuains
019500        if client-ip-type-4-88                                    dfhuains
019600            move zeroes to ws-client-ip-address-v4                dfhuains
019700            move 0 to two-bytes-binary                            dfhuains
019800            move client-ip-address-4(1:1) to two-bytes-right      dfhuains
019900            move two-bytes-binary to ws-client-addr-part1         dfhuains
020000            move 0 to two-bytes-binary                            dfhuains
020100            move client-ip-address-4(2:1) to two-bytes-right      dfhuains
020200            move two-bytes-binary to ws-client-addr-part2         dfhuains
020300            move 0 to two-bytes-binary                            dfhuains
020400            move client-ip-address-4(3:1) to two-bytes-right      dfhuains
020500            move two-bytes-binary to ws-client-addr-part3         dfhuains
020600            move 0 to two-bytes-binary                            dfhuains
020700            move client-ip-address-4(4:1) to two-bytes-right      dfhuains
020800            move two-bytes-binary to ws-client-addr-part4         dfhuains
020900            set address of lk-ip-address                          dfhuains
021000             to address of ws-client-ip-address                   dfhuains
021100            set address of lk-ip-address-v4x                      dfhuains
021200             to address of ws-client-ip-address-v4                dfhuains
021300            perform get-dotted-addr                               dfhuains
021400        end-if                                                    dfhuains
021500     end-if.                                                      dfhuains
021600                                                                  dfhuains
021700*  check if we have a ip-address to work with                     dfhuains
021800*  for the connected server                                       dfhuains
021900                                                                  dfhuains
022000     if address of tn3270-server-ip not = null                    dfhuains
022100         move server-ip-port to ws-server-port                    dfhuains
022200         if server-ip-type-4-88                                   dfhuains
022300             move zeroes to ws-server-ip-address-v4               dfhuains
022400             move 0 to two-bytes-binary                           dfhuains
022500             move server-ip-address-4(1:1) to two-bytes-right     dfhuains
022600             move two-bytes-binary to ws-server-addr-part1        dfhuains
022700             move 0 to two-bytes-binary                           dfhuains
022800             move server-ip-address-4(2:1) to two-bytes-right     dfhuains
022900             move two-bytes-binary to ws-server-addr-part2        dfhuains
023000             move 0 to two-bytes-binary                           dfhuains
023100             move server-ip-address-4(3:1) to two-bytes-right     dfhuains
023200             move two-bytes-binary to ws-server-addr-part3        dfhuains
023300             move 0 to two-bytes-binary                           dfhuains
023400             move server-ip-address-4(4:1) to two-bytes-right     dfhuains
023500             move two-bytes-binary to ws-server-addr-part4        dfhuains
023600         end-if                                                   dfhuains
023700     end-if.                                                      dfhuains
023800                                                                  dfhuains
023900*  Start from A000 to B000 through Z000 then A001 to B001 through dfhuains
024000*  Z001 through A999 to B999 through Z999                         dfhuains
024100*  allows limit of 26 x 999 terminals                             dfhuains
024200                                                                  dfhuains
024300*  Must have low-values in status-byte on return else             dfhuains
024400*  auto install will be rejected.                                 dfhuains
024500                                                                  dfhuains
024600     move low-values to tct-info.                                 dfhuains
024700     if cwa-last-term-id not = low-values                         dfhuains
024800         perform allocate-term-id                                 dfhuains
024900     else                                                         dfhuains
025000         move 'A000' to ws-term-id                                dfhuains
025100     end-if.                                                      dfhuains
025200                                                                  dfhuains
025300*  This program uses the first entry by default but may scan      dfhuains
025400*  for user terminal attributes in the autoinst-name according to dfhuains
025500*  defined convention to find appropriate model to return.        dfhuains
025600                                                                  dfhuains
025700     move model-table-name(1) to tct-info-name.                   dfhuains
025800                                                                  dfhuains
025900*     if term-info-rows = 24 and                                  dfhuains
026000*        term-info-cols = 80                                      dfhuains
026100*        move 'MODMOD2' to tct-info-name                          dfhuains
026200*     end-if.                                                     dfhuains
026300                                                                  dfhuains
026400     move ws-term-id to tct-info-term-id.                         dfhuains
026500     move ws-term-id to cwa-last-term-id.                         dfhuains
026600                                                                  dfhuains
026700     exec cics deq resource(cwa-area)                             dfhuains
026800     end-exec.                                                    dfhuains
026900                                                                  dfhuains
027000     perform display-connection.                                  dfhuains
027100                                                                  dfhuains
027200     exec cics return end-exec.                                   dfhuains
027300                                                                  dfhuains
027400 allocate-term-id section.                                        dfhuains
027500                                                                  dfhuains
027600*  build unique terminal id                                       dfhuains
027700                                                                  dfhuains
027800     move cwa-last-term-id to ws-term-id.                         dfhuains
027900     if ws-term-id-alpha = 'Z'                                    dfhuains
028000        add 1 to ws-term-id-numeric                               dfhuains
028100        move 'A' to ws-term-id-alpha                              dfhuains
028200     else                                                         dfhuains
028300        perform varying ws-sub from 1 by 1                        dfhuains
028400          until ws-term-id-alpha = ws-alpha-char(ws-sub)          dfhuains
028500                 or ws-sub = 26                                   dfhuains
028600         end-perform                                              dfhuains
028700         add 1 to ws-sub                                          dfhuains
028800         move ws-alpha-char(ws-sub) to ws-term-id-alpha           dfhuains
028900     end-if.                                                      dfhuains
029000                                                                  dfhuains
029100                                                                  dfhuains
029200 get-dotted-addr section.                                         dfhuains
029300     move lk-addr-part1x to ws-temp4.                             dfhuains
029400     perform 3 times                                              dfhuains
029500       if ws-temp4(1:1) is equal to space                         dfhuains
029600          move ws-temp4(2:3) to ws-temp4(1:3)                     dfhuains
029700          move space to ws-temp4(4:1)                             dfhuains
029800       end-if                                                     dfhuains
029900     end-perform.                                                 dfhuains
030000     move ws-temp4 to lk-addr-part1x.                             dfhuains
030100     move lk-addr-part2x to ws-temp4.                             dfhuains
030200     perform 3 times                                              dfhuains
030300       if ws-temp4(1:1) is equal to space                         dfhuains
030400          move ws-temp4(2:3) to ws-temp4(1:3)                     dfhuains
030500          move space to ws-temp4(4:1)                             dfhuains
030600       end-if                                                     dfhuains
030700     end-perform.                                                 dfhuains
030800     move ws-temp4 to lk-addr-part2x.                             dfhuains
030900     move lk-addr-part3x to ws-temp4.                             dfhuains
031000     perform 3 times                                              dfhuains
031100       if ws-temp4(1:1) is equal to space                         dfhuains
031200          move ws-temp4(2:3) to ws-temp4(1:3)                     dfhuains
031300          move space to ws-temp4(4:1)                             dfhuains
031400       end-if                                                     dfhuains
031500     end-perform.                                                 dfhuains
031600     move ws-temp4 to lk-addr-part3x.                             dfhuains
031700     move lk-addr-part4x to ws-temp4.                             dfhuains
031800     perform 3 times                                              dfhuains
031900       if ws-temp4(1:1) is equal to space                         dfhuains
032000          move ws-temp4(2:3) to ws-temp4(1:3)                     dfhuains
032100          move space to ws-temp4(4:1)                             dfhuains
032200       end-if                                                     dfhuains
032300     end-perform.                                                 dfhuains
032400     move ws-temp4 to lk-addr-part4x.                             dfhuains
032500     move spaces to lk-ip-address.                                dfhuains
032600     string lk-addr-part1x delimited by space                     dfhuains
032700            '.' delimited by size                                 dfhuains
032800            lk-addr-part2x delimited by space                     dfhuains
032900            '.' delimited by size                                 dfhuains
033000            lk-addr-part3x delimited by space                     dfhuains
033100            '.' delimited by size                                 dfhuains
033200            lk-addr-part4x delimited by space                     dfhuains
033300       into lk-ip-address.                                        dfhuains
033400                                                                  dfhuains
033500                                                                  dfhuains
033600 display-connection section.                                      dfhuains
033700     exec cics asktime abstime(ws-abstime)                        dfhuains
033800     end-exec.                                                    dfhuains
033900     exec cics formattime abstime(ws-abstime)                     dfhuains
034000                          yyyymmdd(ws-date-yyyy-mm-dd)            dfhuains
034100     end-exec.                                                    dfhuains
034200                                                                  dfhuains
034300     move low-values to ws-msg                                    dfhuains
034400     string ws-term-id delimited by size                          dfhuains
034500            ' connected from ' delimited by size                  dfhuains
034600            ws-client-ip-address delimited by space               dfhuains
034700            ' on ' delimited by size                              dfhuains
034800            ws-date-yyyy delimited by size                        dfhuains
034900            '-' delimited by size                                 dfhuains
035000            ws-month(ws-date-mm) delimited by size                dfhuains
035100            '-' delimited by size                                 dfhuains
035200            ws-date-dd delimited by size                          dfhuains
035300            x'00' delimited by size                               dfhuains
035400       into ws-msg.                                               dfhuains
035500     move 0 to ws-srv-cmsg-size.                                  dfhuains
035600     inspect ws-msg tallying ws-srv-cmsg-size                     dfhuains
035700       for characters before x'00'.                               dfhuains
035800     set ws-srv-cmsg-ptr to address of ws-msg.                    dfhuains
035900                                                                  dfhuains
036000     call 78-srv-cmsg-entry-name using ws-srv-console-msg         dfhuains
036100     end-call.                                                    dfhuains
036200                                                                  dfhuains
036300* $ version 5.97a sequenced on tuesday 15 jul 2008 at 12:45am     dfhuains
036400* $ Version 5.99c sequenced on Wednesday 3 Mar 2011 at 1:00pm     dfhuains
