000100***************************************************************** ubnkplt1
000200*                                                               * ubnkplt1
000300*   Copyright (C) 1998-2010 Micro Focus. All Rights Reserved.   * ubnkplt1
000400*   This demonstration program is provided for use by users     * ubnkplt1
000500*   of Micro Focus products and may be used, modified and       * ubnkplt1
000600*   distributed as part of your application provided that       * ubnkplt1
000700*   you properly acknowledge the copyright of Micro Focus       * ubnkplt1
000800*   in this material.                                           * ubnkplt1
000900*                                                               * ubnkplt1
001000***************************************************************** ubnkplt1
001100                                                                  ubnkplt1
001200***************************************************************** ubnkplt1
001300* Program:     UBNKPLT1.CBL (CICS Version)                      * ubnkplt1
001400* Layer:       System level                                     * ubnkplt1
001500* Function:    PLTI Processing (ES/MTO Startup)                 * ubnkplt1
001600*---------------------------------------------------------------* ubnkplt1
001700* PLT Initialisation (PLTI) can run one or more programs once   * ubnkplt1
001800* at system startup or at the initialisation of every SEP.      * ubnkplt1
001900* This is a dummy process to illustrate the "run once" scenario * ubnkplt1
002000***************************************************************** ubnkplt1
002100                                                                  ubnkplt1
002200 IDENTIFICATION DIVISION.                                         ubnkplt1
002300 PROGRAM-ID.                                                      ubnkplt1
002400     UBNKPLT1.                                                    ubnkplt1
002500 DATE-WRITTEN.                                                    ubnkplt1
002600     September 2002.                                              ubnkplt1
002700 DATE-COMPILED.                                                   ubnkplt1
002800     Today.                                                       ubnkplt1
002900                                                                  ubnkplt1
003000 ENVIRONMENT DIVISION.                                            ubnkplt1
003100                                                                  ubnkplt1
003200 DATA DIVISION.                                                   ubnkplt1
003300 WORKING-STORAGE SECTION.                                         ubnkplt1
003400 01  WS-MISC-STORAGE.                                             ubnkplt1
003500   05  WS-PROGRAM-ID                         PIC X(8)             ubnkplt1
003600       VALUE 'UBNKPLT1'.                                          ubnkplt1
003700   05  WS-WTO-DATA.                                               ubnkplt1
003800     10  FILLER                              PIC X(4)             ubnkplt1
003900         VALUE 'INT '.                                            ubnkplt1
004000     10  FILLER                              PIC X(7)             ubnkplt1
004100         VALUE 'Termid:'.                                         ubnkplt1
004200     10  WS-WTO-TERM                         PIC X(4).            ubnkplt1
004300     10  FILLER                              PIC X(9)             ubnkplt1
004400         VALUE ', Tranid:'.                                       ubnkplt1
004500     10  WS-WTO-TRAN                         PIC X(4).            ubnkplt1
004600     10  FILLER                              PIC X(10)            ubnkplt1
004700         VALUE ', Program:'.                                      ubnkplt1
004800     10  WS-WTO-PROG                         PIC X(8).            ubnkplt1
004900                                                                  ubnkplt1
005000 LINKAGE SECTION.                                                 ubnkplt1
005100                                                                  ubnkplt1
005200 PROCEDURE DIVISION.                                              ubnkplt1
005300***************************************************************** ubnkplt1
005400* Display the msg                                               * ubnkplt1
005500***************************************************************** ubnkplt1
005600     MOVE z'UBNKPLT1 Complete' TO WS-WTO-DATA.                    ubnkplt1
005700     EXEC CICS WRITE                                              ubnkplt1
005800               OPERATOR                                           ubnkplt1
005900               TEXT(WS-WTO-DATA)                                  ubnkplt1
006000               TEXTLENGTH(LENGTH OF WS-WTO-DATA)                  ubnkplt1
006100     END-EXEC.                                                    ubnkplt1
006200                                                                  ubnkplt1
006300     EXEC CICS WRITEQ TD                                          ubnkplt1
006400               QUEUE('CSMT')                                      ubnkplt1
006500               FROM(WS-WTO-DATA)                                  ubnkplt1
006600               LENGTH(LENGTH OF WS-WTO-DATA)                      ubnkplt1
006700     END-EXEC.                                                    ubnkplt1
006800                                                                  ubnkplt1
006900***************************************************************** ubnkplt1
007000* Now we have to have finished and can return to our invoker.   * ubnkplt1
007100***************************************************************** ubnkplt1
007200     EXEC CICS                                                    ubnkplt1
007300          RETURN                                                  ubnkplt1
007400     END-EXEC.                                                    ubnkplt1
007500     GOBACK.                                                      ubnkplt1
007600                                                                  ubnkplt1
007700* $ Version 5.99c sequenced on Wednesday 3 Mar 2011 at 1:00pm     ubnkplt1
