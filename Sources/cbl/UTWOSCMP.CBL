000100***************************************************************** utwoscmp
000200*                                                               * utwoscmp
000300*   Copyright (C) 1998-2010 Micro Focus. All Rights Reserved.   * utwoscmp
000400*   This demonstration program is provided for use by users     * utwoscmp
000500*   of Micro Focus products and may be used, modified and       * utwoscmp
000600*   distributed as part of your application provided that       * utwoscmp
000700*   you properly acknowledge the copyright of Micro Focus       * utwoscmp
000800*   in this material.                                           * utwoscmp
000900*                                                               * utwoscmp
001000***************************************************************** utwoscmp
001100                                                                  utwoscmp
001200***************************************************************** utwoscmp
001300* Program:     UTWOSCMP.CBL                                     * utwoscmp
001400* Function:    ??conversion utility routine                     * utwoscmp
001500***************************************************************** utwoscmp
001600                                                                  utwoscmp
001700 IDENTIFICATION DIVISION.                                         utwoscmp
001800 PROGRAM-ID.                                                      utwoscmp
001900     UTWOSCMP.                                                    utwoscmp
002000 DATE-WRITTEN.                                                    utwoscmp
002100     September 2002.                                              utwoscmp
002200 DATE-COMPILED.                                                   utwoscmp
002300     Today.                                                       utwoscmp
002400                                                                  utwoscmp
002500 ENVIRONMENT DIVISION.                                            utwoscmp
002600                                                                  utwoscmp
002700 DATA DIVISION.                                                   utwoscmp
002800 WORKING-STORAGE SECTION.                                         utwoscmp
002900 01  WS-MISC-STORAGE.                                             utwoscmp
003000   05  WS-PROGRAM-ID                         PIC X(8)             utwoscmp
003100       VALUE 'UTWOSCMP'.                                          utwoscmp
003200   05  WS-LEN                                PIC 9(4) COMP.       utwoscmp
003300                                                                  utwoscmp
003400   05  WS-WORK-INPUT.                                             utwoscmp
003500     10  WS-WORK-INPUT-N                     PIC 9(4) COMP.       utwoscmp
003600   05  FILLER REDEFINES WS-WORK-INPUT.                            utwoscmp
003700     10  WS-WORK-INPUT-BYTE-1                PIC X(1).            utwoscmp
003800     10  WS-WORK-INPUT-BYTE-2                PIC X(1).            utwoscmp
003900                                                                  utwoscmp
004000   05  WS-WORK-OUTPUT.                                            utwoscmp
004100     10  WS-WORK-OUTPUT-N                    PIC 9(4) COMP.       utwoscmp
004200   05  FILLER REDEFINES WS-WORK-OUTPUT.                           utwoscmp
004300     10  WS-WORK-OUTPUT-BYTE-1               PIC X(1).            utwoscmp
004400     10  WS-WORK-OUTPUT-BYTE-2               PIC X(1).            utwoscmp
004500                                                                  utwoscmp
004600 LINKAGE SECTION.                                                 utwoscmp
004700 01  LK-TWOS-CMP-LEN                         PIC S9(4) COMP.      utwoscmp
004800 01  LK-TWOS-CMP-INPUT                       PIC X(256).          utwoscmp
004900 01  LK-TWOS-CMP-OUTPUT                      PIC X(256).          utwoscmp
005000                                                                  utwoscmp
005100 PROCEDURE DIVISION USING LK-TWOS-CMP-LEN                         utwoscmp
005200                          LK-TWOS-CMP-INPUT                       utwoscmp
005300                          LK-TWOS-CMP-OUTPUT.                     utwoscmp
005400     PERFORM VARYING WS-LEN FROM 1 BY 1                           utwoscmp
005500       UNTIL WS-LEN > LK-TWOS-CMP-LEN                             utwoscmp
005600       MOVE 0 TO WS-WORK-INPUT-N                                  utwoscmp
005700       MOVE LK-TWOS-CMP-INPUT(WS-LEN:1) TO WS-WORK-INPUT-BYTE-2   utwoscmp
005800       MOVE 255 TO WS-WORK-OUTPUT-N                               utwoscmp
005900       SUBTRACT WS-WORK-INPUT-N FROM WS-WORK-OUTPUT-N             utwoscmp
006000       MOVE WS-WORK-OUTPUT-BYTE-2 TO LK-TWOS-CMP-OUTPUT(WS-LEN:1) utwoscmp
006100     END-PERFORM.                                                 utwoscmp
006200                                                                  utwoscmp
006300     GOBACK.                                                      utwoscmp
006400                                                                  utwoscmp
006500* $ Version 5.99c sequenced on Wednesday 3 Mar 2011 at 1:00pm     utwoscmp
