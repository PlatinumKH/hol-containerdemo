#!/bin/bash
. $MFPRODBASE/bin/cobsetenv

# Setup passwords in the vault to allow dbfhdeploy to connect to the database
script_dir=$(dirname $0)
$script_dir/vaultconfig.sh

# Create and upload the files to the database
$COBDIR/bin/dbfhdeploy create sql://PGSQL/JCLTEST
$COBDIR/bin/dbfhdeploy create sql://PGSQL/CMDTOOLS

$COBDIR/bin/dbfhadmin -script -type:crossregion -provider:pg -file:createcrossregiondb.sql
$COBDIR/bin/dbfhadmin -script -type:region -name:MYPAC -provider:pg -file:createregiondb.sql

$COBDIR/bin/dbfhdeploy add /home/esadm/deploy/catalog/CATALOG.DAT sql://PGSQL/JCLTEST/CATALOG.DAT?folder=/PARSE

$COBDIR/bin/dbfhdeploy add /home/esadm/deploy/catalog/data/MFI01V.MFIDEMO.BNKHELP.dat sql://PGSQL/JCLTEST/MFI01V.MFIDEMO.BNKHELP.dat?folder=/PARSE/data
$COBDIR/bin/dbfhdeploy add /home/esadm/deploy/catalog/data/MFI01V.MFIDEMO.BNKCUST.dat sql://PGSQL/JCLTEST/MFI01V.MFIDEMO.BNKCUST.dat?folder=/PARSE/data
$COBDIR/bin/dbfhdeploy add /home/esadm/deploy/catalog/data/MFI01V.MFIDEMO.BNKTXN.dat sql://PGSQL/JCLTEST/MFI01V.MFIDEMO.BNKTXN.dat?folder=/PARSE/data
$COBDIR/bin/dbfhdeploy add /home/esadm/deploy/catalog/data/MFI01V.MFIDEMO.BNKATYPE.dat sql://PGSQL/JCLTEST/MFI01V.MFIDEMO.BNKATYPE.dat?folder=/PARSE/data
$COBDIR/bin/dbfhdeploy add /home/esadm/deploy/catalog/data/MFI01V.MFIDEMO.BNKACC.dat sql://PGSQL/JCLTEST/MFI01V.MFIDEMO.BNKACC.dat?folder=/PARSE/data

$COBDIR/bin/dbfhdeploy add /home/esadm/deploy/catalog/ctlcards/KBNKSRT1.txt "sql://PGSQL/JCLTEST/KBNKSRT1.txt?folder=/PARSE/ctlcards;type=lseq;reclen=80,80"

$COBDIR/bin/dbfhdeploy data add /home/esadm/deploy/catalog/prc/YBATTSO.prc "sql://PGSQL/JCLTEST/YBATTSO.prc?folder=/PARSE/prc;type=lseq;reclen=80,80"
$COBDIR/bin/dbfhdeploy data add /home/esadm/deploy/catalog/prc/YBNKEXTV.prc "sql://PGSQL/JCLTEST/YBNKEXTV.prc?folder=/PARSE/prc;type=lseq;reclen=80,80"
$COBDIR/bin/dbfhdeploy data add /home/esadm/deploy/catalog/prc/YBNKPRT1.prc "sql://PGSQL/JCLTEST/YBNKPRT1.prc?folder=/PARSE/prc;type=lseq;reclen=80,80"
$COBDIR/bin/dbfhdeploy data add /home/esadm/deploy/catalog/prc/YBNKSRT1.prc "sql://PGSQL/JCLTEST/YBNKSRT1.prc?folder=/PARSE/prc;type=lseq;reclen=80,80"

# Import the Enterprise Server resource definitions to a local file, then upload the file to the database for use by the PAC
$COBDIR/bin/caspcrd /c /dp=/home/esadm/deploy/RDEF
$COBDIR/bin/casrdtup -q -ue:dfhurdupxml -e -f/home/esadm/deploy/${ES_SERVER}_sit.xml -v -o -op/home/esadm/deploy/RDEF
$COBDIR/bin/casrdtup -q -ue:dfhurdupxml -e -f/home/esadm/deploy/${ES_SERVER}_stul.xml -v -o -op/home/esadm/deploy/RDEF
$COBDIR/bin/casrdtup -q -ue:dfhurdupxml -e -f/home/esadm/deploy/${ES_SERVER}_grps.xml -v -o -op/home/esadm/deploy/RDEF

$COBDIR/bin/dbfhdeploy add /home/esadm/deploy/RDEF/dfhdrdat sql://PGSQL/CMDTOOLS/dfhdrdat?folder=/RDEF

export PGUSER=$DB_USERNAME
echo "*:*:*:*:$DB_PASSWORD" > ~/.pgpass
chmod 600 ~/.pgpass
psql -f createcrossregiondb.sql
psql -f createregiondb.sql
