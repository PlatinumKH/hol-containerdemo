#!/bin/bash

# Request the region is stopped
. $MFPRODBASE/bin/cobsetenv && casstop -r$ES_SERVER -u`mfsecretsadmin read microfocus/CAS/casstart-user` -p`mfsecretsadmin read microfocus/CAS/casstart-password`

# Loop until the server has stopped
while [ ! -f /var/mfcobol/es/BANKDEMO/shutdown.txt ]; do sleep 3; done;