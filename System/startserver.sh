#!/bin/bash

. $MFPRODBASE/bin/cobsetenv

# Setup LDAP trusted root certificate
echo TLS_CACERT $OPENLDAP_CAROOT > ~/.ldaprc

# Start MFDS and wait until it is running
echo Starting MFDS
$COBDIR/bin/mfds64 &
MFDS_PID=$!
while ! curl --output /dev/null --silent --fail http://127.0.0.1:$CCITCP2_PORT; do sleep 1; done;

# Wait for the DB to be available
echo Waiting for DB to come on-line
until $COBDIR/bin/dbfhdeploy list sql://PGSQL/JCLTEST; do echo waiting for DB; sleep 2; done;

# Wait for the local syslog server to be available - it receives audit events
if ! [ -z "$SYSLOG_PORT" ]
then
  echo Waiting for syslog sidecar to come on-line
  until logger --server localhost --port $SYSLOG_PORT --tcp --priority user.info Enterprise Server starting; do echo waiting for local syslog; sleep 2; done;
fi

# Remove previous log
if [ -e /var/mfcobol/es/$ES_SERVER/console.log ]
then
    rm /var/mfcobol/es/$ES_SERVER/console.log
fi

# Remove previous mfcs log
if [ -e /var/mfcobol/es/BANKDEMO/log.html ]
then
    rm /var/mfcobol/es/BANKDEMO/log*.html
fi

# Remove the file used to signal shutdown is complete
if [ -e /var/mfcobol/es/$ES_SERVER/shutdown.txt ]
then 
    rm /var/mfcobol/es/$ES_SERVER/shutdown.txt
fi

# Start the Enterpise Server
echo Starting the Enterprise Server region
$COBDIR/bin/casstart /R$ES_SERVER /S:C /U`mfsecretsadmin read microfocus/CAS/casstart-user` /P`mfsecretsadmin read microfocus/CAS/casstart-password`

# Wait for console log to be created signalling the server is running
while [ ! -f /var/mfcobol/es/$ES_SERVER/console.log ]; do echo waiting for server to start; sleep 3; done; 

# Output the console log until the ES daemon terminates
CASCD_PID=`pgrep cascd64`
tail -n+1 --pid=$CASCD_PID -F /var/mfcobol/es/$ES_SERVER/console.log

# Signal that the server has now shutdown
touch /var/mfcobol/es/$ES_SERVER/shutdown.txt
