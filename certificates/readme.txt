To install certificate in cluster follow these steps:

1. Install Helm V3
2. Commands:
   kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml
   # Create the namespace for cert-manager
   kubectl create namespace cert-manager

   # Add the Jetstack Helm repository
   helm repo add jetstack https://charts.jetstack.io

   # Update your local Helm chart repository cache
   helm repo update

   # Install the cert-manager Helm chart
   helm install <name> --namespace cert-manager --version v0.11.0 jetstack/cert-manager

To verify installation run:
kubectl get pods --namespace cert-manager

which should look like:

NAME                                       READY   STATUS    RESTARTS   AGE
cert-manager-5cf55d9b65-lvvrc              1/1     Running   0          24h
cert-manager-cainjector-7f84c8bf48-n9jv7   1/1     Running   0          24h
cert-manager-webhook-7cc658d5c4-m2bkr      1/1     Running   0          24h

3. kubectl create secret tls ca-key-pair --cert=ca.crt --key=ca.key --namespace=default
4. kubectl apply -f <deploymentdir>\tls-certificates.yaml
5. Import ca.crt into your browser root certification authority trust store

Alternatively - to use fixed server certificates (without cert-manager) use:
kubectl create secret generic example-org-tls --from-file=tls.key --from-file=tls.crt --from-file=ca.crt
kubectl create secret generic bankdemo-ldap-tls --from-file=tls.key --from-file=tls.crt --from-file=ca.crt