# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
& {
    Get-Date

    #$env:path += 'C:\Users\user\.azure-kubectl'

    # Install AWS CLI msi from https://s3.amazonaws.com/aws-cli/AWSCLI64PY3.msi

    # Install EKSCTL for Windows - requires chocolatey

    # Run aws configure with private key info from your account

    if ("$PROJECTNAME" -eq "") {
    Write-Output "No project name passed as parameter"
    exit 1
    }

    $EC2_AVAILABILITY_ZONES=aws ec2 describe-availability-zones | ConvertFrom-Json

    $EC2_REGION_NAME=$EC2_AVAILABILITY_ZONES.AvailabilityZones[0].RegionName
    $EC2_ZONE_NAME=$EC2_AVAILABILITY_ZONES.AvailabilityZones[0].ZoneName
    $EC2_ZONE_NAME_1=$EC2_AVAILABILITY_ZONES.AvailabilityZones[1].ZoneName
    $EC2_ZONE_NAME_2=$EC2_AVAILABILITY_ZONES.AvailabilityZones[2].ZoneName
    $EC2_ZONE_NAME_3=$EC2_AVAILABILITY_ZONES.AvailabilityZones[3].ZoneName
    $EC2_ZONE_NAME_4=$EC2_AVAILABILITY_ZONES.AvailabilityZones[4].ZoneName
    $EC2_ZONE_NAME_5=$EC2_AVAILABILITY_ZONES.AvailabilityZones[5].ZoneName

    Write-Output "$($EC2_REGION_NAME) $($EC2_ZONE_NAME) $($EC2_ZONE_NAME_1) $($EC2_ZONE_NAME_2) $($EC2_ZONE_NAME_3) $($EC2_ZONE_NAME_4) $($EC2_ZONE_NAME_5) "

    $EKS_CLUSTER_NAME="$($PROJECTNAME)"
    Write-Output "Cluster Name is: $($EKS_CLUSTER_NAME)"
    #NIY - not sure why we're doing this
    #$EKS_CLUSTER_NAME=`sed -e 's/^"//' -e 's/"$//' <<<"$EKS_CLUSTER_NAME"`

    $RDS_DB_SUBNET_GRP_NAME="$($PROJECTNAME)-dbsubnetgroup"
    Write-Output "Subnet Group Name is: $($RDS_DB_SUBNET_GRP_NAME)"

    $RDS_DB_CLUSTER_NAME="$PROJECTNAME-db"
    Write-Output "DB Cluster Name is: $($RDS_DB_CLUSTER_NAME)"

    # DB name should have ONLY alphanumeric characters
    $RDS_DB_NAME="pgsql$($PROJECTNAME)"
    $RDS_DB_INSTANCE=$("$($PROJECTNAME)-instance")

    Write-Output "DB Info: $($RDS_DB_NAME) $($RDS_DB_INSTANCE)"

    $ELASTICACHE_SNG_NAME="$PROJECTNAME-redis-sg"
    $ELASTICACHE_CLUSTER_ID=$("$($PROJECTNAME)-redis-clust")

    Write-Output "Elasticache info: $($ELASTICACHE_SNG_NAME) $($ELASTICACHE_CLUSTER_ID)"

    Write-Output "EKS Cluster Name is " $EKS_CLUSTER_NAME

    eksctl get cluster --name ${EKS_CLUSTER_NAME} --output json >$null
    if ($LASTEXITCODE -ne "0") {
        Write-Output "cluster doesnt exist."
    }

    aws rds describe-db-subnet-groups --db-subnet-group-name ${RDS_DB_SUBNET_GRP_NAME} >$null
    if ($LASTEXITCODE -ne "0") {
        Write-Output "db subnet group doesnt exist"
    }

    aws rds describe-db-clusters --db-cluster-identifier ${RDS_DB_CLUSTER_NAME} >$null
    if ($LASTEXITCODE -ne "0") {
        Write-Output "db cluster doesnt exist."
        $RDS_DB_CLUSTER_EXISTS="no"
    }

    aws rds describe-db-instances --db-instance-identifier ${RDS_DB_INSTANCE} >$null
    if ($LASTEXITCODE -ne "0") {
        Write-Output "db instance doesnt exist."
        $RDS_DB_INSTANCE_EXISTS="no"
    }

    aws elasticache describe-cache-subnet-groups --cache-subnet-group-name ${ELASTICACHE_SNG_NAME} >$null
    if ($LASTEXITCODE -ne "0") {
        Write-Output "redis subnet group doesnt exist."
    }

    aws elasticache describe-cache-clusters --cache-cluster-id ${ELASTICACHE_CLUSTER_ID} >$null
    if ($LASTEXITCODE -ne "0") {
        Write-Output "redis cluster doesnt exist."
        $ELASTICACHE_CLUSTER_EXISTS="no"
    }

    # Actually start deleting stuff - prompt for user confirmation
    $abort = New-Object System.Management.Automation.Host.ChoiceDescription '&Abort','Aborts the operation'
    $continue = New-Object System.Management.Automation.Host.ChoiceDescription '&Continue','Carries on the operation'
    $options = [System.Management.Automation.Host.ChoiceDescription[]] ($abort,$continue)
    $choice = $host.ui.PromptForChoice("This will delete your entire cluster and associated AWS infrastructure for $($PROJECTNAME), please confirm you want to proceed","[A]bort or [C]ontinue",$options,0)

    if ($choice -ne "1") {
        Write-Output "Aborting"
        exit 1
    }

    aws rds delete-db-instance --db-instance-identifier ${RDS_DB_INSTANCE} --skip-final-snapshot
    if ($LASTEXITCODE -ne "0") {
        Write-Output "DB instance wasn't deleted."
    }

    if ($RDS_DB_INSTANCE_EXISTS -eq "0") {
        do { Write-Output "Waiting for DB instance deletion"; Start-Sleep -seconds 5; $RDS_DB_INST_DEL=aws rds describe-db-instances --db-instance-identifier ${RDS_DB_INSTANCE} } until (-not ([string]::IsNullOrEmpty($($RDS_DB_INST_DEL))) )
    }

    if ($RDS_DB_CLUSTER_EXISTS -ne "no") {
        aws rds delete-db-cluster --db-cluster-identifier ${RDS_DB_CLUSTER_NAME} --skip-final-snapshot
        if ($LASTEXITCODE -ne "0") {
            Write-Output "DB Cluster wasn't deleted."
        }
        do { Write-Output "Waiting for DB Cluster deletion"; Start-Sleep -seconds 5; $RDS_DB_CLUST_DEL=aws rds describe-db-clusters --db-cluster-identifier $($RDS_DB_CLUSTER_NAME); } until ([string]::IsNullOrEmpty($($RDS_DB_CLUST_DEL)) )
    } else {
        Write-Output "DB Cluster doesn't exist so skipping..."
    }

    aws rds delete-db-subnet-group --db-subnet-group-name ${RDS_DB_SUBNET_GRP_NAME}
    if ($LASTEXITCODE -ne "0") {
        Write-Output "DB subnet group wasn't deleted."
    }

    if ($ELASTICACHE_CLUSTER_EXISTS -ne "no") {
        aws elasticache delete-cache-cluster --cache-cluster-id ${ELASTICACHE_CLUSTER_ID}
        if ($LASTEXITCODE -ne "0") {
            Write-Output "Redis Elasticache cluster wasn't deleted."
        }
        do { Write-Output "Waiting for Elasticache Redis deletion"; Start-Sleep -seconds 5; $ELASTICACHE_CLUST_DEL=aws elasticache describe-cache-clusters --cache-cluster-id $($ELASTICACHE_CLUSTER_ID) } until ([string]::IsNullOrEmpty($($ELASTICACHE_CLUST_DEL)) )
        aws elasticache delete-cache-subnet-group --cache-subnet-group-name ${ELASTICACHE_SNG_NAME}
        if ($LASTEXITCODE -ne "0") {
            Write-Output "Redis Elasticache subnet wasn't deleted."
        } else {
            Write-Output "redis subnet deleted..."
        }
    }
    Get-Date
    eksctl delete cluster --name ${EKS_CLUSTER_NAME} -w --timeout 60m0s
    if ($LASTEXITCODE -ne "0") {
        Write-Output "EKS cluster wasn't deleted."
    }

    Get-Date
}