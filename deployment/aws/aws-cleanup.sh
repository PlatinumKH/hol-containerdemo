set -x #echo on

#aws ecr delete-repository --force --repository-name bankdemo
#aws ecr delete-repository --force --repository-name openldap
#aws ecr delete-repository --force --repository-name escwa

export PROJECTNAME=$1

if [ "$PROJECTNAME" == "" ]; then
  echo "no project name passed as parameter"
  set +x #echo on
  exit 1
fi

export EC2_REGION_NAME=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[0].RegionName'`
export EC2_REGION_NAME=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_REGION_NAME"`
export EC2_ZONE_NAME=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[0].ZoneName'`
export EC2_ZONE_NAME=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME"`
export EC2_ZONE_NAME_1=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[1].ZoneName'`
export EC2_ZONE_NAME_1=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME_1"`
export EC2_ZONE_NAME_2=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[2].ZoneName'`
export EC2_ZONE_NAME_2=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME_2"`
export EC2_ZONE_NAME_3=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[3].ZoneName'`
export EC2_ZONE_NAME_3=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME_3"`
export EC2_ZONE_NAME_4=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[4].ZoneName'`
export EC2_ZONE_NAME_4=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME_4"`
export EC2_ZONE_NAME_5=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[5].ZoneName'`
export EC2_ZONE_NAME_5=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME_5"`

#########################################################

export EKS_CLUSTER_NAME=$PROJECTNAME
export EKS_CLUSTER_NAME=`sed -e 's/^"//' -e 's/"$//' <<<"$EKS_CLUSTER_NAME"`

export RDS_DB_SUBNET_GRP_NAME=$PROJECTNAME-dbsubnetgroup
export RDS_DB_CLUSTER_NAME=$PROJECTNAME-db
# DB name should have ONLY alphanumeric characters
export RDS_DB_NAME=pgsql$PROJECTNAME
export RDS_DB_INSTANCE=$PROJECTNAME-instance

export ELASTICACHE_SNG_NAME=$PROJECTNAME-redis-sg
export ELASTICACHE_CLUSTER_ID=$PROJECTNAME-redis-clust

eksctl get cluster --name ${EKS_CLUSTER_NAME} --output json
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
  echo "cluster doesnt exist."
fi

aws rds describe-db-subnet-groups --db-subnet-group-name ${RDS_DB_SUBNET_GRP_NAME}
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
  echo "db subnet group doesnt exist"
fi

aws rds describe-db-clusters --db-cluster-identifier ${RDS_DB_CLUSTER_NAME}
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
  echo "db cluster doesnt exist."
fi

aws rds describe-db-instances --db-instance-identifier ${RDS_DB_INSTANCE}
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
  echo "db instance doesnt exist."
fi

aws elasticache describe-cache-subnet-groups --cache-subnet-group-name ${ELASTICACHE_SNG_NAME}
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "redis subnet group doesnt exist."
fi

aws elasticache describe-cache-clusters --cache-cluster-id ${ELASTICACHE_CLUSTER_ID}
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "redis cluster doesnt exist."
fi

aws rds delete-db-instance --db-instance-identifier ${RDS_DB_INSTANCE} --skip-final-snapshot

export RDS_DB_INST_DEL=`aws rds describe-db-instances --db-instance-identifier ${RDS_DB_INSTANCE}`
while [ "$RDS_DB_INST_DEL" != "" ]
do
sleep 30
export RDS_DB_INST_DEL=`aws rds describe-db-instances --db-instance-identifier ${RDS_DB_INSTANCE}`
done

aws rds delete-db-cluster --db-cluster-identifier ${RDS_DB_CLUSTER_NAME} --skip-final-snapshot

export RDS_DB_CLUST_DEL=`aws rds describe-db-clusters --db-cluster-identifier ${RDS_DB_CLUSTER_NAME}`
while [ "$RDS_DB_CLUST_DEL" != "" ]
do
sleep 30
export RDS_DB_CLUST_DEL=`aws rds describe-db-clusters --db-cluster-identifier ${RDS_DB_CLUSTER_NAME}`
done

aws rds delete-db-subnet-group --db-subnet-group-name ${RDS_DB_SUBNET_GRP_NAME}

aws elasticache delete-cache-cluster --cache-cluster-id ${ELASTICACHE_CLUSTER_ID}

export ELASTICACHE_CLUST_DEL=`aws elasticache describe-cache-clusters --cache-cluster-id ${ELASTICACHE_CLUSTER_ID}`
while [ "$ELASTICACHE_CLUST_DEL" != "" ]
do
sleep 30
export ELASTICACHE_CLUST_DEL=`aws elasticache describe-cache-clusters --cache-cluster-id ${ELASTICACHE_CLUSTER_ID}`
done

aws elasticache delete-cache-subnet-group --cache-subnet-group-name ${ELASTICACHE_SNG_NAME}

date
eksctl delete cluster --name ${EKS_CLUSTER_NAME} -w --timeout 60m0s
date

set +x #echo on
