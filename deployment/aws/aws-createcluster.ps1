# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
& {
Get-Date

#$env:path += 'C:\Users\user\.azure-kubectl'

# Install AWS CLI msi from https://s3.amazonaws.com/aws-cli/AWSCLI64PY3.msi

# Install EKSCTL for Windows - requires chocolatey

# Run aws configure with private key info from your account

#Install Helm if not available
try {
    helm.exe version
} catch {
    #Helm isn't available, so downloading Helm
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    Invoke-WebRequest -Uri https://get.helm.sh/helm-v3.0.0-windows-amd64.zip -OutFile $env:TEMP\helm.zip
    Expand-Archive -force $env:TEMP\helm.zip $env:TEMP\helm\
    $env:path += ";$($env:HOME)\.azure-kubectl;$($env:TEMP)\helm\windows-amd64"
}

if ("$PROJECTNAME" -eq "") {
  Write-Output "No project name passed as parameter"
  exit 1
}

Remove-Item $PSScriptRoot\metrics*zip
Get-ChildItem -Path $PSScriptRoot\metrics* -Recurse | Remove-Item -force -recurse
Remove-Item $PSScriptRoot\metrics* -force
Remove-Item $PSScriptRoot\cloudp*yaml
Remove-Item $PSScriptRoot\..\cloudp*yaml
Remove-Item $PSScriptRoot\aws*yaml
Remove-Item $PSScriptRoot\..\aws*yaml

& $PSScriptRoot\aws-preparecluster.ps1 $PROJECTNAME
if ($LASTEXITCODE -ne "0") {
    exit $LASTEXITCODE
}

#az acr repository list --name <acr_registry_name> --output json | ConvertFrom-Json
#az acr repository list --name niybasicregistry --output json | ConvertFrom-Json

$awsEcrRepositories=aws ecr describe-repositories | Convertfrom-Json

foreach ( $repository in $awsEcrRepositories.repositories )  {
    #Write-Host $repository.repositoryName
    if ($($repository.repositoryName) -eq 'bankdemo') {
        $bankdemoRepositorySource = $($repository.repositoryUri)
    }
    if ($($repository.repositoryName) -eq 'openldap') {
        $openldapRepositorySource = $($repository.repositoryUri)
    }
    if ($($repository.repositoryName) -eq 'escwa') {
        $escwaRepositorySource = $($repository.repositoryUri)
    }
    if ($($repository.repositoryName) -eq 'es-metrics') {
        $metricsRepositorySource = $($repository.repositoryUri)
    }
    if ($($repository.repositoryName) -eq 'kubeproxy') {
        $kproxyRepositorySource = $($repository.repositoryUri)
    }
}
((Get-Content -path $PSScriptRoot\..\..\escwa\escwa.yaml -Raw) -creplace "niybasicregistry.azurecr.io/escwa","$escwaRepositorySource") | Set-Content -Path $PSScriptRoot\..\..\escwa\cloudp-escwa1.yaml
((Get-Content -path $PSScriptRoot\..\..\escwa\cloudp-escwa1.yaml -Raw) -creplace "niybasicregistry.azurecr.io/kubeproxy","$kproxyRepositorySource") | Set-Content -Path $PSScriptRoot\..\..\escwa\cloudp-escwa.yaml
((Get-Content -path $PSScriptRoot\..\backend.yaml -Raw) -creplace "niybasicregistry.azurecr.io/bankdemo","$bankdemoRepositorySource") | Set-Content -Path $PSScriptRoot\..\cloudp-backend1.yaml
((Get-Content -path $PSScriptRoot\..\cloudp-backend1.yaml -Raw) -creplace "niybasicregistry.azurecr.io/es-metrics","$metricsRepositorySource") | Set-Content -Path $PSScriptRoot\..\cloudp-backend.yaml
((Get-Content -path $PSScriptRoot\..\openldap.yaml -Raw) -creplace "niybasicregistry.azurecr.io/openldap","$openldapRepositorySource") | Set-Content -Path $PSScriptRoot\..\cloudp-openldap.yaml
((Get-Content -path $PSScriptRoot\..\initdb.yaml -Raw) -creplace "niybasicregistry.azurecr.io/bankdemo","$bankdemoRepositorySource") | Set-Content -Path $PSScriptRoot\..\cloudp-initdb.yaml

& $PSScriptRoot\aws-setupcluster.ps1 $PROJECTNAME
if ($LASTEXITCODE -ne "0") {
    exit $LASTEXITCODE
}

kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install com-microfocus-certificates --namespace cert-manager --version v0.11.0 jetstack/cert-manager

kubectl create secret tls ca-key-pair --cert=$PSScriptRoot\..\..\certificates\ca.crt --key=$PSScriptRoot\..\..\certificates\ca.key --namespace=default
kubectl wait --for=condition=ready --timeout=300s --namespace cert-manager --all=true pod
do { Start-sleep -Seconds 3 } until (kubectl apply -f $PSScriptRoot\..\tls-certificates.yaml | Where-Object { $? } )
if ($LASTEXITCODE -ne "0") {
    Write-Host "Cert-manager failed to start. Exit code $LASTEXITCODE"
    exit 1
}

#deploy prometheus adapter using helm
helm install -f $PSScriptRoot\..\..\metrics\adaptervalues.yaml test-release stable/prometheus-adapter --set prometheus.url=http://prometheus-server.prometheus.svc --set prometheus.port=80 --set logLevel=6
kubectl wait --for=condition=ready --timeout=300s --all=true pod
if ($LASTEXITCODE -ne "0") {
    Write-Host "Prometheus adapter failed to start. Exit code $LASTEXITCODE"
    exit 1
}

#Install the kubeernetes dashboard so the pod usage can be visualised
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml
kubectl apply -f $PSScriptRoot\..\eks-admin-service-account.yaml

$kubeadmintoken = ($(kubectl -n kube-system get secret) | Select-String eks-admin) -split ' '
#Use the token from the following command to log in (or use the config file from the users .kube location)
kubectl -n kube-system describe secret $kubeadmintoken[0]

#Run the following command to allow access to kubernetes dashboard
#kubectl proxy

#Open a browser Window to the following URL and enter the token that is displayed during the dashboard deploy
#http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#!/login


& $PSScriptRoot\aws-installimages.ps1 $PROJECTNAME user

Get-Date
}