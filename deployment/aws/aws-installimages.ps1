$PROJECTNAME=($args[0]).ToLower()
$NSPACE=($args[1]).ToLower()
& {
Get-Date

kubectl create namespace $NSPACE-ns
kubectl config set-context --current --namespace=$NSPACE-ns

#apply certificates from certificates directory (see readme.txt)
kubectl create secret tls ca-key-pair --cert=$PSScriptRoot\..\..\certificates\ca.crt --key=$PSScriptRoot\..\..\certificates\ca.key
kubectl wait --for=condition=ready --namespace cert-manager --all=true pod
do { Start-sleep -Seconds 3 } until (kubectl apply -f $PSScriptRoot\..\tls-certificates.yaml | Where-Object { $? } )

kubectl create secret generic mfdbfh-config --from-file=MFDBFH.cfg=$PSScriptRoot\..\..\secrets\cloudp-MFDBFH.cfg

#cd to deployment directory and apply az-secrets.yaml
((Get-Content -path $PSScriptRoot\..\secrets.yaml -Raw) -creplace "mfdbfh","microfocusadmin") | Set-Content -Path $PSScriptRoot\cloudp-secrets.yaml
kubectl apply -f $PSScriptRoot\cloudp-secrets.yaml

kubectl apply -f $PSScriptRoot\cloudp-pgsql-service-deploy.yaml

#initialise db
kubectl create configmap ed60-bnkd-odbc-config   --from-file=odbc.ini=$PSScriptRoot\..\config\odbc.ini
kubectl apply -f $PSScriptRoot\..\cloudp-initdb.yaml
kubectl wait --for=condition=Complete --timeout=600s --selector='job-name=init-mfdbfh' job
if ($LASTEXITCODE -ne "0") {
  Write-Output initdbfh job failed, exiting
  exit 1
}
kubectl delete job init-mfdbfh

$namespaceString='"Namespace":"default"'
$newNamespaceString='"Namespace":"' + "$NSPACE-ns" + '"'
Move-Item $PSScriptRoot\..\..\escwa\cloudp-escwa.yaml $PSScriptRoot\..\..\escwa\cloudp-escwa2.yaml
((Get-Content -path $PSScriptRoot\..\..\escwa\cloudp-escwa2.yaml -Raw) -creplace $namespaceString,$newNamespaceString) | Set-Content -Path $PSScriptRoot\..\..\escwa\cloudp-escwa.yaml

#run az-demo.bat
kubectl apply -f $PSScriptRoot\..\rolebinding.yaml
kubectl apply -f $PSScriptRoot\..\cloudp-openldap.yaml
kubectl apply -f $PSScriptRoot\cloudp-redis-service-deploy.yaml
kubectl apply -f $PSScriptRoot\..\syslog.yaml
kubectl wait --for=condition=ready --all=true pod --timeout=180s
kubectl create configmap ed60-bnkd-syslog-config --from-file=rsyslog.conf=$PSScriptRoot\..\config\rsyslog.conf
kubectl apply -f $PSScriptRoot\..\cloudp-backend.yaml
kubectl apply -f $PSScriptRoot\..\hpa.yaml
kubectl apply -f $PSScriptRoot\..\..\escwa\cloudp-escwa.yaml

kubectl get all

Get-Date
}