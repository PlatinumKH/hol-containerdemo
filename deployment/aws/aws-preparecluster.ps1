# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
& {
Get-Date

#$env:path += 'C:\Users\user\.azure-kubectl'

# Install AWS CLI msi from https://s3.amazonaws.com/aws-cli/AWSCLI64PY3.msi

# Install EKSCTL for Windows - requires chocolatey

# Run aws configure with private key info from your account

if ("$PROJECTNAME" -eq "") {
  Write-Output "No project name passed as parameter"
  exit 1
}

$EC2_AVAILABILITY_ZONES=aws ec2 describe-availability-zones | ConvertFrom-Json

$EC2_REGION_NAME=$EC2_AVAILABILITY_ZONES.AvailabilityZones[0].RegionName
$EC2_ZONE_NAME=$EC2_AVAILABILITY_ZONES.AvailabilityZones[0].ZoneName
$EC2_ZONE_NAME_1=$EC2_AVAILABILITY_ZONES.AvailabilityZones[1].ZoneName
$EC2_ZONE_NAME_2=$EC2_AVAILABILITY_ZONES.AvailabilityZones[2].ZoneName
$EC2_ZONE_NAME_3=$EC2_AVAILABILITY_ZONES.AvailabilityZones[3].ZoneName
$EC2_ZONE_NAME_4=$EC2_AVAILABILITY_ZONES.AvailabilityZones[4].ZoneName
$EC2_ZONE_NAME_5=$EC2_AVAILABILITY_ZONES.AvailabilityZones[5].ZoneName

Write-Output "$($EC2_REGION_NAME) $($EC2_ZONE_NAME) $($EC2_ZONE_NAME_1) $($EC2_ZONE_NAME_2) $($EC2_ZONE_NAME_3) $($EC2_ZONE_NAME_4) $($EC2_ZONE_NAME_5) "

$EKS_CLUSTER_NAME="$($PROJECTNAME)"
Write-Output "Cluster Name is: $($EKS_CLUSTER_NAME)"
#NIY - not sure why we're doing this
#$EKS_CLUSTER_NAME=`sed -e 's/^"//' -e 's/"$//' <<<"$EKS_CLUSTER_NAME"`

$RDS_DB_SUBNET_GRP_NAME="$($PROJECTNAME)-dbsubnetgroup"
Write-Output "Subnet Group Name is: $($RDS_DB_SUBNET_GRP_NAME)"

$RDS_DB_CLUSTER_NAME="$PROJECTNAME-db"
Write-Output "DB Cluster Name is: $($RDS_DB_CLUSTER_NAME)"

# DB name should have ONLY alphanumeric characters
$RDS_DB_NAME="pgsql$($PROJECTNAME)"
$RDS_DB_INSTANCE=$("$($PROJECTNAME)-instance")

Write-Output "DB Info: $($RDS_DB_NAME) $($RDS_DB_INSTANCE)"

$ELASTICACHE_SNG_NAME="$PROJECTNAME-redis-sg"
$ELASTICACHE_CLUSTER_ID=$("$($PROJECTNAME)-redis-clust")

Write-Output "Elasticache info: $($ELASTICACHE_SNG_NAME) $($ELASTICACHE_CLUSTER_ID)"

Write-Output "EKS Cluster Name is $EKS_CLUSTER_NAME"
eksctl get cluster --name $($EKS_CLUSTER_NAME) --output json >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "cluster already exists. Delete existing cluster before runnign this script"
  exit 2
}

aws rds describe-db-subnet-groups --db-subnet-group-name $($RDS_DB_SUBNET_GRP_NAME) >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "DB subnet group already exists. Delete existing db subnet group before running this script"
  exit 3
}

aws rds describe-db-clusters --db-cluster-identifier ${RDS_DB_CLUSTER_NAME} >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "db cluster already exists. Delete existing db cluster before running this script"
  exit 4
}

aws rds describe-db-instances --db-instance-identifier ${RDS_DB_INSTANCE} >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "DB instance already exists. Delete existing db instance before running this script"
  exit 5
}

aws elasticache describe-cache-subnet-groups --cache-subnet-group-name ${ELASTICACHE_SNG_NAME} >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "redis subnet group already exists. pass new argument or delete existing redis subnet group"
  exit 6
}

aws elasticache describe-cache-clusters --cache-cluster-id ${ELASTICACHE_CLUSTER_ID} >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "redis cluster already exists. pass new argument or delete existing redis cluster"
  exit 7
}

######## this is to be used only when there isnt an existing VPC where you want to create your cluster ###########

eksctl create cluster --name ${EKS_CLUSTER_NAME} --version 1.14 --region ${EC2_REGION_NAME} --nodegroup-name ${EKS_CLUSTER_NAME}-nodegroup --managed --node-type t2.medium --nodes 2 --nodes-min 1 --nodes-max 3
if ($LASTEXITCODE -ne "0") {
  Write-Output "Redis cluster creation failed."
  exit 8
}

$EKS_CLUSTER_OBJECT=eksctl get cluster --name $($EKS_CLUSTER_NAME) --output json | ConvertFrom-Json
Write-Output "Cluster Object is $($EKS_CLUSTER_OBJECT)"

$EKS_SECURITY_GRP_ID=$EKS_CLUSTER_OBJECT.ResourcesVpcConfig.ClusterSecurityGroupId
$EKS_SUBNET_1=$($EKS_CLUSTER_OBJECT.ResourcesVpcConfig.SubnetIds[0])
$EKS_SUBNET_2=$($EKS_CLUSTER_OBJECT.ResourcesVpcConfig.SubnetIds[1])
$EKS_SUBNET_3=$($EKS_CLUSTER_OBJECT.ResourcesVpcConfig.SubnetIds[2])
$EKS_SUBNET_4=$($EKS_CLUSTER_OBJECT.ResourcesVpcConfig.SubnetIds[3])

Write-Output "Security and Subnet Info: $($EKS_SECURITY_GRP_ID) $($EKS_SUBNET_1) $($EKS_SUBNET_2) $($EKS_SUBNET_3) $($EKS_SUBNET_4)"

# create DB subnet group

aws rds create-db-subnet-group --db-subnet-group-name  $($RDS_DB_SUBNET_GRP_NAME) --db-subnet-group-description "bank Demo DB Subnet Group" --subnet-ids $($EKS_SUBNET_1) $($EKS_SUBNET_2) $($EKS_SUBNET_3) $($EKS_SUBNET_4) #| jq '.DBSubnetGroup.DBSubnetGroupName'
if ($LASTEXITCODE -ne "0") {
  Write-Output "DB subnet group creation failed."
  exit 9
}

# create DB cluster
aws rds create-db-cluster --db-cluster-identifier $($RDS_DB_CLUSTER_NAME) --engine aurora-postgresql --engine-version 11.4 --port 5432 --master-username microfocusadmin --master-user-password MfdbfhAdmin1 --database-name $($RDS_DB_NAME) --db-subnet-group-name $($RDS_DB_SUBNET_GRP_NAME) --vpc-security-group-ids $($EKS_SECURITY_GRP_ID) --no-storage-encrypted --no-enable-iam-database-authentication
if ($LASTEXITCODE -ne "0") {
  Write-Output "DB cluster creation failed."
  exit 10
}

# create db instance
aws rds create-db-instance --db-instance-identifier $($RDS_DB_INSTANCE) --db-cluster-identifier $($RDS_DB_CLUSTER_NAME) --engine aurora-postgresql --db-instance-class db.t3.medium --db-subnet-group-name $($RDS_DB_SUBNET_GRP_NAME) --no-multi-az
if ($LASTEXITCODE -ne "0") {
  Write-Output "DB instance creation failed."
  exit 11
}

# allow access to pg db from withing the vpc. open port 5432
aws ec2 authorize-security-group-ingress --group-id $($EKS_SECURITY_GRP_ID) --protocol tcp --port 5432 --cidr 0.0.0.0/0
if ($LASTEXITCODE -ne "0") {
  Write-Output "Adding rule to access db port failed."
  exit 12
}

#########################################################

# create redis subnet group

aws elasticache create-cache-subnet-group --cache-subnet-group-name $($ELASTICACHE_SNG_NAME) --cache-subnet-group-description "$($PROJECTNAME)-redistest" --subnet-ids $($EKS_SUBNET_1) $($EKS_SUBNET_2) $($EKS_SUBNET_3) $($EKS_SUBNET_4)
if ($LASTEXITCODE -ne "0") {
  Write-Output "redis subnet group creation failed."
  exit 13
}

# create redis cluster
aws elasticache create-cache-cluster --cache-cluster-id $($ELASTICACHE_CLUSTER_ID) --cache-node-type cache.t2.small --engine redis --engine-version 5.0.3 --cache-subnet-group-name $($ELASTICACHE_SNG_NAME) --security-group-ids $($EKS_SECURITY_GRP_ID) --port 6379 --num-cache-nodes 1
if ($LASTEXITCODE -ne "0") {
  Write-Output "redis cluster creation failed."
  exit 14
}

# allow access to redis from withing the vpc. open port 6379
aws ec2 authorize-security-group-ingress --group-id $($EKS_SECURITY_GRP_ID) --protocol tcp --port 6379 --cidr 0.0.0.0/0
if ($LASTEXITCODE -ne "0") {
  Write-Output "adding rule to access redis port failed."
  exit 15
}


Get-Date
}