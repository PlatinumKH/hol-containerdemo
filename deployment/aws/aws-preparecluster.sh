set -x #echo on

# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
export PROJECTNAME=`echo $1 | awk '{print tolower($0)}'`

if [ "$PROJECTNAME" == "" ]; then
  echo "no project name passed as parameter"
  set +x #echo on
  exit 1
fi

export EC2_AVAILABILITY_ZONES=`aws ec2 describe-availability-zones`
export EC2_REGION_NAME=`echo $EC2_AVAILABILITY_ZONES | jq -r '.AvailabilityZones[0].RegionName'`
export EC2_ZONE_NAME=`echo $EC2_AVAILABILITY_ZONES | jq -r '.AvailabilityZones[0].ZoneName'`
export EC2_ZONE_NAME_1=`echo $EC2_AVAILABILITY_ZONES | jq -r '.AvailabilityZones[1].ZoneName'`
export EC2_ZONE_NAME_2=`echo $EC2_AVAILABILITY_ZONES | jq -r '.AvailabilityZones[2].ZoneName'`
export EC2_ZONE_NAME_3=`echo $EC2_AVAILABILITY_ZONES | jq -r '.AvailabilityZones[3].ZoneName'`
export EC2_ZONE_NAME_4=`echo $EC2_AVAILABILITY_ZONES | jq -r '.AvailabilityZones[4].ZoneName'`
export EC2_ZONE_NAME_5=`echo $EC2_AVAILABILITY_ZONES | jq -r '.AvailabilityZones[5].ZoneName'`

export EKS_CLUSTER_NAME=$PROJECTNAME

export RDS_DB_SUBNET_GRP_NAME=$PROJECTNAME-dbsubnetgroup
export RDS_DB_CLUSTER_NAME=$PROJECTNAME-db

# DB name should have ONLY alphanumeric characters
export RDS_DB_NAME=pgsql$PROJECTNAME
export RDS_DB_INSTANCE=$PROJECTNAME-instance

export ELASTICACHE_SNG_NAME=$PROJECTNAME-redis-sg
export ELASTICACHE_CLUSTER_ID=$PROJECTNAME-redis-clust

eksctl get cluster --name ${EKS_CLUSTER_NAME} --output json
export ERROR_CODE=$?

if [ "$ERROR_CODE" == "0" ]
then
  echo "cluster already exists. pass new argument or delete existing cluster"
  set +x #echo on
  exit 2
fi

aws rds describe-db-subnet-groups --db-subnet-group-name ${RDS_DB_SUBNET_GRP_NAME}
export ERROR_CODE=$?

if [ "$ERROR_CODE" == "0" ]
then
  echo "db subnet group already exists. pass new argument or delete existing db subnet group"
  set +x #echo on
  exit 3
fi

aws rds describe-db-clusters --db-cluster-identifier ${RDS_DB_CLUSTER_NAME}
export ERROR_CODE=$?

if [ "$ERROR_CODE" == "0" ]
then
  echo "db cluster already exists. pass new argument or delete existing db cluster"
  set +x #echo on
  exit 4
fi

aws rds describe-db-instances --db-instance-identifier ${RDS_DB_INSTANCE}
export ERROR_CODE=$?

if [ "$ERROR_CODE" == "0" ]
then
  echo "db instance already exists. pass new argument or delete existing db instance"
  set +x #echo on
  exit 5
fi

aws elasticache describe-cache-subnet-groups --cache-subnet-group-name ${ELASTICACHE_SNG_NAME}
export ERROR_CODE=$?

if [ "$ERROR_CODE" == "0" ]
then
  echo "redis subnet group already exists. pass new argument or delete existing redis subnet group"
  set +x #echo on
  exit 6
fi

aws elasticache describe-cache-clusters --cache-cluster-id ${ELASTICACHE_CLUSTER_ID}
export ERROR_CODE=$?

if [ "$ERROR_CODE" == "0" ]
then
  echo "redis cluster already exists. pass new argument or delete existing redis cluster"
  set +x #echo on
  exit 7
fi


######## this is to be used only when there isnt an existing VPC where you want to create your cluster ###########
if ! [[ "$NODECOUNT" =~ ^[0-9]+$ ]]; then
    export NODECOUNT=2
fi
eksctl create cluster --name ${EKS_CLUSTER_NAME} --version 1.14 --region ${EC2_REGION_NAME} --nodegroup-name ${EKS_CLUSTER_NAME}-nodegroup --managed --node-type t2.medium --nodes $NODECOUNT
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "redis cluster creation failed."
  set +x #echo on
  exit 8
fi

export EKS_CLUSTER_INFO=`eksctl get cluster --name ${EKS_CLUSTER_NAME} --output json`

# get the Security group and subnets created as part of EKS creation
export EKS_SECURITY_GRP_ID=`echo $EKS_CLUSTER_INFO | jq -r '.[0].ResourcesVpcConfig.ClusterSecurityGroupId'`

export EKS_SUBNET_1=`echo $EKS_CLUSTER_INFO | jq -r '.[0].ResourcesVpcConfig.SubnetIds[0]'`
export EKS_SUBNET_2=`echo $EKS_CLUSTER_INFO | jq -r '.[0].ResourcesVpcConfig.SubnetIds[1]'`
export EKS_SUBNET_3=`echo $EKS_CLUSTER_INFO | jq -r '.[0].ResourcesVpcConfig.SubnetIds[2]'`
export EKS_SUBNET_4=`echo $EKS_CLUSTER_INFO | jq -r '.[0].ResourcesVpcConfig.SubnetIds[3]'`

#########################################################
#export RDS_DB_SUBNET_GRP_NAME=`aws rds create-db-subnet-group --db-subnet-group-name  ${RDS_DB_SUBNET_GRP_NAME} --db-subnet-group-description "bank Demo DB Subnet Group" --subnet-ids ${EKS_SUBNET_1} ${EKS_SUBNET_2} ${EKS_SUBNET_3} ${EKS_SUBNET_4} | jq -r '.DBSubnetGroup.DBSubnetGroupName'`

# create db subnet group
aws rds create-db-subnet-group --db-subnet-group-name  ${RDS_DB_SUBNET_GRP_NAME} --db-subnet-group-description "bank Demo DB Subnet Group" --subnet-ids ${EKS_SUBNET_1} ${EKS_SUBNET_2} ${EKS_SUBNET_3} ${EKS_SUBNET_4}
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "db subnet group creation failed."
  set +x #echo on
  exit 9
fi

# create db cluster
aws rds create-db-cluster --db-cluster-identifier ${RDS_DB_CLUSTER_NAME} --engine aurora-postgresql --engine-version 11.4 --port 5432 --master-username microfocusadmin --master-user-password MfdbfhAdmin1 --database-name ${RDS_DB_NAME} --db-subnet-group-name ${RDS_DB_SUBNET_GRP_NAME} --vpc-security-group-ids ${EKS_SECURITY_GRP_ID} --no-storage-encrypted --no-enable-iam-database-authentication
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "db cluster creation failed."
  set +x #echo on
  exit 10
fi

# create db instance
aws rds create-db-instance --db-instance-identifier ${RDS_DB_INSTANCE} --db-cluster-identifier ${RDS_DB_CLUSTER_NAME} --engine aurora-postgresql --db-instance-class db.t3.medium --db-subnet-group-name ${RDS_DB_SUBNET_GRP_NAME} --no-multi-az
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "db instance creation failed."
  set +x #echo on
  exit 11
fi

# allow access to pg db from withing the vpc. open port 5432
aws ec2 authorize-security-group-ingress --group-id ${EKS_SECURITY_GRP_ID} --protocol tcp --port 5432 --cidr 0.0.0.0/0
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "adding rule to access db port failed."
  set +x #echo on
  exit 12
fi

#########################################################

# create redis subnet group
aws elasticache create-cache-subnet-group --cache-subnet-group-name ${ELASTICACHE_SNG_NAME} --cache-subnet-group-description ${PROJECTNAME}-redistest --subnet-ids ${EKS_SUBNET_1} ${EKS_SUBNET_2} ${EKS_SUBNET_3} ${EKS_SUBNET_4}
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "redis subnet group creation failed."
  set +x #echo on
  exit 13
fi

# create redis cluster
aws elasticache create-cache-cluster --cache-cluster-id ${ELASTICACHE_CLUSTER_ID} --cache-node-type cache.t2.small --engine redis --engine-version 5.0.3 --cache-subnet-group-name ${ELASTICACHE_SNG_NAME} --security-group-ids ${EKS_SECURITY_GRP_ID} --port 6379 --num-cache-nodes 1
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "redis cluster creation failed."
  set +x #echo on
  exit 14
fi

# allow access to redis from withing the vpc. open port 6379
aws ec2 authorize-security-group-ingress --group-id ${EKS_SECURITY_GRP_ID} --protocol tcp --port 6379 --cidr 0.0.0.0/0
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "adding rule to access redis port failed."
  set +x #echo on
  exit 15
fi

#########################################################


set +x #echo on
