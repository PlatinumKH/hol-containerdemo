# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
& {
Get-Date

$RDS_DB_INSTANCE=$("$($PROJECTNAME)-instance")
$ELASTICACHE_CLUSTER_ID=$("$($PROJECTNAME)-redis-clust")

Write-Output "Waiting for Redis Endpoint"
do { Write-Output "Waiting for redis cluster endpoint"; Start-Sleep -seconds 3; $redisEndpoint=aws elasticache describe-cache-clusters --cache-cluster-id ${ELASTICACHE_CLUSTER_ID} --show-cache-node-info | ConvertFrom-Json } until (-not ([string]::IsNullOrEmpty($($redisEndpoint.CacheClusters.CacheNodes.Endpoint.Address))) )
Write-Output "Redis Endpoint name is " $($redisEndpoint.CacheClusters.CacheNodes.Endpoint.Address)

((Get-Content -path $PSScriptRoot\..\cloud-redis-service.yaml -Raw) -creplace "cloudredisendpointname","$($redisEndpoint.CacheClusters.CacheNodes.Endpoint.Address)") | Set-Content -Path $PSScriptRoot\cloudp-redis-service-deploy.yaml

#cd to azure secrets and apply secret config
(Get-Content $PSScriptRoot\..\..\secrets\MFDBFH.cfg) -creplace "mfdbfh","microfocusadmin" -join "`n" | Set-Content -NoNewline -Force $PSScriptRoot\..\..\secrets\cloudp-MFDBFH.cfg

#Apply the settings required to point to external db service name
do { Write-Output "Waiting for DB Endpoint"; Start-Sleep -seconds 3; $dbEndpoint=aws rds describe-db-instances --db-instance-identifier ${RDS_DB_INSTANCE} | ConvertFrom-Json } until (-not ([string]::IsNullOrEmpty($($dbEndpoint.DBInstances.Endpoint.Address))) )
Write-Output "DB Endpoint name is " "$($dbEndpoint.DBInstances.Endpoint.Address)"

((Get-Content -path $PSScriptRoot\..\cloud-pgsql-service.yaml -Raw) -creplace "cloudpgsqlendpointname","$($dbEndpoint.DBInstances.Endpoint.Address)") | Set-Content -Path $PSScriptRoot\cloudp-pgsql-service-deploy.yaml

#start metrics server
$response = Invoke-RestMethod -Uri "https://api.github.com/repos/kubernetes-sigs/metrics-server/releases/latest"
$response | Get-Member
$DOWNLOAD_URL=$response.zipball_url
$DOWNLOAD_VERSION=$response.tag_name
Invoke-RestMethod -Uri "$($DOWNLOAD_URL)" -OutFile metrics-server-$DOWNLOAD_VERSION.zip
Expand-Archive -force metrics-server-$DOWNLOAD_VERSION.zip
Push-Location $PSScriptRoot\metrics-server-$DOWNLOAD_VERSION\*\
kubectl apply -f .\deploy\1.8+\
Pop-Location
kubectl wait --for=condition=ready --timeout=300s --namespace kube-system --all=true pod
kubectl get deployment metrics-server -n kube-system

#deploy prometheus using Helm
kubectl create namespace prometheus
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm repo update
Push-Location $PSScriptRoot\..\..\metrics
helm install prometheus stable/prometheus --namespace prometheus --set alertmanager.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2" --set-file extraScrapeConfigs=extraScrapeConfigs.yaml
Pop-Location
kubectl wait --for=condition=ready --timeout=300s --namespace prometheus --all=true pod
if ($LASTEXITCODE -ne "0") {
    Write-Host "Prometheus failed to start. Exit code $LASTEXITCODE"
    exit 1
}

Get-Date
}