& {
    Get-Date
    
    #Read in settings values from side text file (settings.txt)
    $configSettings = Get-Content .\az-settings.json | ConvertFrom-Json
    Write-Output $configSettings
    
    $curForeground = $($host.ui.rawui.foregroundcolor)
    $wslist=az monitor log-analytics workspace list --resource-group $($configSettings.Kubernetes.AppResourceGroup) | ConvertFrom-Json
    az monitor log-analytics workspace delete --resource-group $($configSettings.Kubernetes.AppResourceGroup) --workspace-name $($wslist.name)
    $host.ui.rawui.foregroundcolor = $($curForeground)

    az group delete --resource-group $($configSettings.Kubernetes.AppResourceGroup) --yes
}