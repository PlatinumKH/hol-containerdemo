& {
Get-Date

#Read in settings values from side text file (settings.txt)
$configSettings = Get-Content .\az-settings.json | ConvertFrom-Json
Write-Output $configSettings

$subscription = az account show | ConvertFrom-Json
$subscriptionId = $subscription.id

try {
    kubectl.exe version
} catch {
    Write-Output "kubectl not installed"
    # configure powershell
    az aks install-cli
}
try {
    helm.exe version
} catch {
    #Helm isn't available, so downloading Helm
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    Invoke-WebRequest -Uri https://get.helm.sh/helm-v3.0.0-windows-amd64.zip -OutFile $env:TEMP\helm.zip
    Expand-Archive -force $env:TEMP\helm.zip $env:TEMP\helm\
    $env:path += ";$($env:HOME)\.azure-kubectl;$($env:TEMP)\helm\windows-amd64"
}

# This section creates a new resource group for the application to run under, and creates a new kubernetes cluster:
try {
    az group create --location $($configSettings.Kubernetes.AzLocation) --name $($configSettings.Kubernetes.AppResourceGroup)

    #Create new Kube cluster and configure the local kubectl instance to work with it.

    if ($($configSettings.DatabaseConfig.DBSkuName) -eq "B_Gen5_1") {
        Write-Output "Basic Database types do not support extended vnet configurations"
    } else {
        az network vnet create --resource-group $($configSettings.Kubernetes.AppResourceGroup) --name $($configSettings.NetworkSettings.AzVnetName) --address-prefixes 192.168.0.0/16 -l $($configSettings.Kubernetes.AzLocation)
        az network vnet subnet create --resource-group $($configSettings.Kubernetes.AppResourceGroup) --name $($configSettings.NetworkSettings.AzSubnetName) --vnet-name $($configSettings.NetworkSettings.AzVnetName)  --address-prefix 192.168.1.0/24 --service-endpoints Microsoft.SQL
        $SUBNET_ID=$(az network vnet subnet show --resource-group $($configSettings.Kubernetes.AppResourceGroup) --vnet-name $($configSettings.NetworkSettings.AzVnetName) --name $($configSettings.NetworkSettings.AzSubnetName) --query id -o tsv)
        $NETWORK_CONFIG="--vnet-subnet-id $($SUBNET_ID) --service-cidr 10.0.0.0/16 --network-plugin kubenet --dns-service-ip 10.0.0.10 --pod-cidr 10.244.0.0/16 --docker-bridge-address 172.17.0.1/16"
        $NETWORK_OPTIONS=$NETWORK_CONFIG.Split(" ")
    }

    #Create a monitoring workspace the name is unique using your subscription ID - each name must be unique, so this allows only one per subscription, also uses day of year as workspaces are soft-delete
    $dayOfYear = $(GET-DATE).DayOfYear
    $monitorWorkspaceName = "$($configSettings.Kubernetes.AppResourceGroup)-ws-$($subscriptionId)-$($dayOfYear)"
    $curForeground = $($host.ui.rawui.foregroundcolor)
    $workspaceSettings = $(az monitor log-analytics workspace create --resource-group $($configSettings.Kubernetes.AppResourceGroup) --workspace-name $($monitorWorkspaceName) | ConvertFrom-Json)
    $host.ui.rawui.foregroundcolor = $($curForeground)

    #Create the cluster
    az aks create --resource-group $($configSettings.Kubernetes.AppResourceGroup) --name $($configSettings.Kubernetes.KubernetesClusterName) --kubernetes-version $($configSettings.Kubernetes.KubernetesVersion) --location $($configSettings.Kubernetes.AzLocation) --node-count $($configSettings.Kubernetes.NumberOfNodes) --node-osdisk-size $($configSettings.Kubernetes.NodeOsDiskSize) --node-vm-size $($configSettings.Kubernetes.NodeVMSize) --generate-ssh-keys $($NETWORK_OPTIONS) --enable-addons monitoring --workspace-resource-id $($workspaceSettings.id)

    #Get the credentials from the newly created cluster
    az aks get-credentials --resource-group $($configSettings.Kubernetes.AppResourceGroup) --name $($configSettings.Kubernetes.KubernetesClusterName) --overwrite-existing
} catch {
    Write-Output "Failure creating cluster."
    exit 1
}

$AzRegistryPasswordObj = (az acr credential show --name $($configSettings.AzureConfig.AzContainerRegistry)) | ConvertFrom-Json
if ($LASTEXITCODE -ne "0") {
    # Look for local override file containing the credentials required to access the container registry
    $RegistryCredentialSettings = Get-Content .\$($configSettings.AzureConfig.AzContainerRegistry).json | ConvertFrom-Json
	Write-Output $RegistryCredentialSettings
    $AzRegistryPassword = $RegistryCredentialSettings.AzureConfig.AzRegistryPassword
    $AzContainerUserName = $RegistryCredentialSettings.AzureConfig.AzContainerUserName
} else {
    $AzRegistryPassword = $AzRegistryPasswordObj.passwords[0].value
    $AzContainerUserName = $AzRegistryPasswordObj.username
}
#Create secret for the container registry based on the values held in the settings config file.
kubectl create secret docker-registry $($configSettings.AzureConfig.AzContainerRegistry) --docker-server=$($configSettings.AzureConfig.AzContainerRegistryServer) --docker-username=$AzContainerUserName --docker-password=$AzRegistryPassword --docker-email=$($configSettings.AzureConfig.AzContainerUserEmail)

#Create a database name so that it is unique using your subscription ID - each name must be unique, so this allows only one per subscription.
$dbName = "$($configSettings.Kubernetes.AppResourceGroup)-db-$subscriptionId"
Write-Output "Database name is:" $dbName

#create postgres server
az postgres server create --name $dbName --resource-group $($configSettings.Kubernetes.AppResourceGroup) --admin-user $($configSettings.DatabaseConfig.DBAdminUser) --admin-password $($configSettings.DatabaseConfig.DBAdminPassword) --sku-name $($configSettings.DatabaseConfig.DBSkuName) --ssl-enforcement Enabled --version $($configSettings.DatabaseConfig.DBVersion)
if ($LASTEXITCODE -ne "0") {
    Write-Output "Failure creating Postgres Database"
}

if ($($configSettings.DatabaseConfig.DBSkuName) -eq "B_Gen5_1") {
    #Update security settings for db to allow access from cluster
    #Note - basic postgress databases are accessible from all Azure instances, do not use this in production
    az postgres server firewall-rule create --resource-group $($configSettings.Kubernetes.AppResourceGroup) --server-name $dbName --name AllowAllWindowsAzureIps --start-ip-address 0.0.0.0 --end-ip-address 0.0.0.0
} else {
    # Use the commands below as an alternative to those above to secure the database to just the bankdemo application
    # see https://docs.microsoft.com/en-us/azure/postgresql/concepts-firewall-rules and https://docs.microsoft.com/en-us/azure/postgresql/howto-manage-vnet-using-cli
    Write-Output "Applying firewall settings"
    az postgres server vnet-rule create --name bankdemo-rule --resource-group $($configSettings.Kubernetes.AppResourceGroup) --server-name $dbName --vnet-name $($configSettings.NetworkSettings.AzVnetName) --subnet $($configSettings.NetworkSettings.AzSubnetName)
}

#Everything from this point on should be generic to any cloud provider

#Install the kubeernetes dashboard so the pod usage can be visualised
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml
kubectl apply -f $PSScriptRoot\..\eks-admin-service-account.yaml

$kubeadmintoken = ($(kubectl -n kube-system get secret) | Select-String eks-admin) -split ' '
#Use the token from the following command to log in (or use the config file from the users .kube location)
kubectl -n kube-system describe secret $kubeadmintoken[0]

#Run the following command to allow access to kubernetes dashboard
#kubectl proxy

#Open a browser Window to the following URL and enter the token that is displayed during the dashboard deploy
#http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#!/login

#cd to azure secrets and apply secret config
(Get-Content $PSScriptRoot\..\..\secrets\MFDBFH.cfg) -creplace "mfdbfh","microfocusadmin@$dbName" -join "`n" | Set-Content -NoNewline -Force $PSScriptRoot\..\..\secrets\az-MFDBFH.cfg
kubectl create secret generic mfdbfh-config --from-file=MFDBFH.cfg=$PSScriptRoot\..\..\secrets\az-MFDBFH.cfg

#cd to deployment directory and apply az-secrets.yaml
((Get-Content -path $PSScriptRoot\..\secrets.yaml -Raw) -creplace "mfdbfh","microfocusadmin@$dbname") | Set-Content -Path $PSScriptRoot\az-secrets.yaml
kubectl apply -f $PSScriptRoot\az-secrets.yaml

#Apply the settings required to point to external db service name
((Get-Content -path $PSScriptRoot\..\cloud-pgsql-service.yaml -Raw) -creplace "cloudpgsqlendpointname","$dbName.postgres.database.azure.com") | Set-Content -Path $PSScriptRoot\az-pgsql-service.yaml

kubectl apply -f $PSScriptRoot\az-pgsql-service.yaml

#initialise db
kubectl create configmap ed60-bnkd-odbc-config   --from-file=odbc.ini=$PSScriptRoot\..\config\odbc.ini
kubectl apply -f $PSScriptRoot\..\initdb.yaml
kubectl wait --for=condition=Complete --timeout=600s --selector='job-name=init-mfdbfh' job
kubectl delete job init-mfdbfh
if ($LASTEXITCODE -ne "0") {
    Write-Output initdbfh job failed, exiting
    exit 1
}

#apply certificates from certificates directory (see readme.txt)

kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install com-microfocus-certificates --namespace cert-manager --version v0.11.0 jetstack/cert-manager

kubectl create secret tls ca-key-pair --cert=$PSScriptRoot\..\..\certificates\ca.crt --key=$PSScriptRoot\..\..\certificates\ca.key --namespace=default
kubectl wait --for=condition=ready --namespace cert-manager --all=true pod
do { Start-Sleep 3 } until (kubectl apply -f $PSScriptRoot\..\tls-certificates.yaml | Where-Object { $? } )

#run az-demo.bat
kubectl apply -f $PSScriptRoot\..\rolebinding.yaml
kubectl apply -f $PSScriptRoot\..\openldap.yaml
kubectl apply -f $PSScriptRoot\..\redis.yaml
kubectl apply -f $PSScriptRoot\..\syslog.yaml
kubectl wait --for=condition=ready --all=true pod --timeout=180s
kubectl create configmap ed60-bnkd-syslog-config --from-file=rsyslog.conf=$PSScriptRoot\..\config\rsyslog.conf
kubectl apply -f $PSScriptRoot\..\backend.yaml
kubectl apply -f $PSScriptRoot\..\..\escwa\escwa.yaml
kubectl get all
Get-Date
}