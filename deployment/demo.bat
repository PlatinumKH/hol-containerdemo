kubectl apply -f espgsql.yaml
pause
kubectl apply -f initdb.yaml
kubectl wait --for=condition=Complete --timeout=600s --selector='job-name=init-mfdbfh' job
pause
kubectl apply -f rolebinding.yaml
pause
kubectl apply -f openldap.yaml
pause
kubectl apply -f redis.yaml
pause
kubectl apply -f syslog.yaml
pause
kubectl wait --for=condition=ready --all=true pod --timeout=180s
kubectl apply -f backend.yaml
pause
kubectl apply -f ..\escwa\escwa.yaml
pause
kubectl get all
