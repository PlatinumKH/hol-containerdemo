cleanup_services () {
    kubectl get namespace | grep -v "NAME" | awk '{ print $1}' | while read line
    do
        serviceName=$(kubectl get service -n $line | grep -v "CLUSTER-IP" | awk '{ print $1}')
        if [ "$serviceName" != "" ]; then
            echo "Deleting service $serviceName in namespace $line"
            kubectl delete service $serviceName -n $line
        fi
    done
}


set -x #echo on

export PROJECTNAME=`echo $1 | awk '{print tolower($0)}'`
export ROLE_ID=`curl http://169.254.169.254/latest/meta-data/iam/security-credentials`
./aws-configurecli.sh

#Running twice to make sure nothing left lying around
cleanup_services
sleep 10
cleanup_services
aws cloudformation delete-stack --stack-name "$PROJECTNAME-count-macro"
export ERROR_CODE=$?
if [ "$ERROR_CODE" != "0" ]; then
    echo "Stack $PROJECTNAME-count-macro deletion failed!"
fi

reposNames=( "bankdemo" "openldap" "escwa" "es-metrics" "kubeproxy" "microfocus/entserver" "microfocus/entdevhub" )
for i in "${reposNames[@]}"; do
    aws ecr delete-repository --repository-name "$PROJECTNAME-$i" --force
    export ERROR_CODE=$?
    if [ "$ERROR_CODE" != "0" ]; then
        echo "Repository $i creation failed"
    fi
done
../aws/aws-cleanup.sh $PROJECTNAME
