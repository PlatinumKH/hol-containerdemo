set -x #echo on

export PROJECTNAME=`echo $1 | awk '{print tolower($0)}'`
export REGION=$2
export ROLEID=$3
export USERCOUNT=$4

export SCRIPT_DIR=`pwd`
aws configure set output json
aws configure set region $REGION
$SCRIPT_DIR/aws-configurecli.sh
aws configure set aws_access_key_id `echo $ROLE | jq -r '.AccessKeyId'`
aws configure set aws_secret_access_key `echo $ROLE | jq -r '.SecretAccessKey'`
aws configure set aws_security_token `echo $ROLE | jq -r '.Token'`
aws configure set aws_session_token `echo $ROLE | jq -r '.Token'`

if ! [[ "$USERCOUNT" =~ ^[0-9]+$ ]]; then
    echo "Non-numeric user count specified"
    set +x
    exit 1
fi

export NODECOUNT=$(($USERCOUNT / 2 + 1))
if [[ $NODECOUNT < 2 ]]; then
    export NODECOUNT=2
fi
$SCRIPT_DIR/../aws/aws-preparecluster.sh $PROJECTNAME
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
    echo "Lab cluster preparation failed."
    set +x #echo on
    exit 1
fi

export adminBucket="amc-hol-docker-images"
export myAccount=$(aws sts get-caller-identity | jq -r '.Account')
export adminAccount='323977192729'
if [ "$myAccount" != "$adminAccount" ]; then
    export bucketName="$adminBucket-$myAccount"
    aws s3api create-bucket --bucket "$bucketName" --create-bucket-configuration LocationConstraint=$REGION #Might already exist

    aws s3 sync "s3://$adminBucket" "s3://$bucketName"
    export ERROR_CODE=$?
    if [ "$ERROR_CODE" != "0" ]; then
        echo "Failed to synchronise s3 buckets!"
        exit 2
    fi
else
    export bucketName=$adminBucket
fi

mkdir /home/administrator/dockerimages
cd /home/administrator/dockerimages
export objects=`aws s3api list-objects --bucket $bucketName`
export ERROR_CODE=$?
if [ "$ERROR_CODE" != "0" ]; then
    echo "Failed to load objects"
    exit 3
fi

for k in $(echo $objects | jq -r '.Contents[] | @base64' ); do
    key=`echo $k | base64 --decode | jq -r '.Key'`
    aws s3 cp s3://$bucketName/$key .
    docker load -i $key
    export ERROR_CODE=$?
    if [ "$ERROR_CODE" != "0" ]; then
        echo "Failed to load docker image $key"
        exit 4
    fi
done

cd -

reposNames=( "bankdemo" "openldap" "escwa" "es-metrics" "kubeproxy" "microfocus/entserver" "microfocus/entdevhub" )
for i in "${reposNames[@]}"; do
    aws ecr create-repository --repository-name "$PROJECTNAME-$i"
    export ERROR_CODE=$?
    if [ "$ERROR_CODE" != "0" ]; then
        echo "Repository $i creation failed"
        set +x #echo on
        exit 5
    fi
done

$(aws ecr get-login --no-include-email)
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
    echo "Docker login failed!"
    set +x #echo on
    exit 6
fi

export reposUri=$(aws ecr describe-repositories --repository-names $PROJECTNAME-bankdemo | jq -r '.repositories[0].repositoryUri' | sed s/$PROJECTNAME-bankdemo//)
docker images | grep -v "login" | grep -v "$reposUri" | grep -v "IMAGE ID" | while read line
do
    name=$(echo "$line" | awk '{print $1}')
    tag=$(echo "$line" | awk '{print $2}')
    docker tag $name:$tag $reposUri$PROJECTNAME-$name
    docker push $reposUri$PROJECTNAME-$name
    if [ "$ERROR_CODE" != "0" ]; then
        echo "Failed to push $reposUri$PROJECTNAME-$name"
        exit 6
    fi
done

cd /home/administrator
git clone https://github.com/awslabs/aws-cloudformation-templates.git
cd ./aws-cloudformation-templates/aws/services/CloudFormation/MacrosExamples/Count
aws cloudformation package --template-file template.yaml --s3-bucket amc-hol-cft-templates --output-template-file packaged.yaml
aws cloudformation deploy --stack-name "$PROJECTNAME-count-macro" --template-file packaged.yaml  --capabilities CAPABILITY_IAM
cd -

echo "Getting ROLE ARN for $ROLEID"
ROLEARN=`aws iam get-role --role-name $ROLEID | jq -r '.Role.Arn'`
eksctl create iamidentitymapping --cluster $PROJECTNAME --arn $ROLEARN --group system:masters

#deploy metrics server on eks
DOWNLOAD_URL=$(curl -Ls "https://api.github.com/repos/kubernetes-sigs/metrics-server/releases/latest" | jq -r .tarball_url)
DOWNLOAD_VERSION=$(grep -o '[^/v]*$' <<< $DOWNLOAD_URL)
curl -Ls $DOWNLOAD_URL -o metrics-server-$DOWNLOAD_VERSION.tar.gz
mkdir metrics-server-$DOWNLOAD_VERSION
tar -xzf metrics-server-$DOWNLOAD_VERSION.tar.gz --directory metrics-server-$DOWNLOAD_VERSION --strip-components 1
kubectl apply -f metrics-server-$DOWNLOAD_VERSION/deploy/1.8+/
kubectl get deployment metrics-server -n kube-system

#deploy prometheus using Helm
kubectl create namespace prometheus
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm repo update
cd $SCRIPT_DIR/../../metrics
helm install prometheus stable/prometheus --namespace prometheus --set alertmanager.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2" --set-file extraScrapeConfigs=extraScrapeConfigs.yaml
kubectl wait --for=condition=ready --namespace prometheus --all=true pod --timeout=100s

kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install com-microfocus-certificates --namespace cert-manager --version v0.11.0 jetstack/cert-manager

kubectl create secret tls ca-key-pair --cert=$SCRIPT_DIR/../../certificates/ca.crt --key=$SCRIPT_DIR/../../certificates/ca.key
kubectl wait --for=condition=ready --namespace cert-manager --all=true pod

until kubectl apply -f $SCRIPT_DIR/../tls-certificates.yaml > /dev/null; do
    sleep 3
done

#deploy prometheus adapter using helm
helm install -f $SCRIPT_DIR/../../metrics/adaptervalues.yaml test-release stable/prometheus-adapter --set prometheus.url=http://prometheus-server.prometheus.svc --set prometheus.port=80 --set logLevel=6
kubectl wait --for=condition=ready --all=true pod --timeout=60s

#Install the kubernetes dashboard so the pod usage can be visualised
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml
kubectl apply -f $SCRIPT_DIR/../eks-admin-service-account.yaml

#Use the token from the following command to log in (or use the config file from the users .kube location)
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')

#Run the following command to allow access to kubernetes dashboard
#kubectl proxy

#Open a browser Window to the following URL and enter the token that is displayed during the dashboard deploy
#http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#!/login

#########################################################

set +x #echo on