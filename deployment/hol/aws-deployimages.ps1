# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
& {
Get-Date

#$env:path += 'C:\Users\user\.azure-kubectl'

# Install AWS CLI msi from https://s3.amazonaws.com/aws-cli/AWSCLI64PY3.msi

# Install EKSCTL for Windows - requires chocolatey

# Run aws configure with private key info from your account

if ("$PROJECTNAME" -eq "") {
  Write-Output "No project name passed as parameter"
  exit 1
}

$ROLE_ID=(curl http://169.254.169.254/latest/meta-data/iam/security-credentials).Content
$ROLE=(curl "http://169.254.169.254/latest/meta-data/iam/security-credentials/$ROLE_ID").Content | ConvertFrom-Json
aws configure set aws_access_key_id $($ROLE.AccessKeyId)
aws configure set aws_secret_access_key $($ROLE.SecretAccessKey)
aws configure set aws_session_token $($ROLE.Token)

aws eks describe-cluster --name $PROJECTNAME
if ($LASTEXITCODE -ne "0") {
    Write-Output "Could not resolve cluster $PROJECTNAME"
    exit 1
}

$awsEcrRepositories=aws ecr describe-repositories | Convertfrom-Json
#$awsEcrRepositories.repositories | Out-Host
foreach ( $repository in $awsEcrRepositories.repositories )  {
    #Write-Host $repository.repositoryName
    if ($($repository.repositoryName) -eq "$PROJECTNAME-bankdemo") {
        $bankdemoRepositorySource = $($repository.repositoryUri)
    }
    if ($($repository.repositoryName) -eq "$PROJECTNAME-openldap") {
        $openldapRepositorySource = $($repository.repositoryUri)
    }
    if ($($repository.repositoryName) -eq "$PROJECTNAME-escwa") {
        $escwaRepositorySource = $($repository.repositoryUri)
    }
    if ($($repository.repositoryName) -eq "$PROJECTNAME-es-metrics") {
        $metricsRepositorySource = $($repository.repositoryUri)
    }
    if ($($repository.repositoryName) -eq "$PROJECTNAME-kubeproxy") {
        $kproxyRepositorySource = $($repository.repositoryUri)
    }
}
((Get-Content -path $PSScriptRoot\..\..\escwa\escwa.yaml -Raw) -creplace "niybasicregistry.azurecr.io/escwa","$escwaRepositorySource") | Set-Content -Path $PSScriptRoot\..\..\escwa\aws-escwa1.yaml
((Get-Content -path $PSScriptRoot\..\..\escwa\aws-escwa1.yaml -Raw) -creplace "niybasicregistry.azurecr.io/kubeproxy","$kproxyRepositorySource") | Set-Content -Path $PSScriptRoot\..\..\escwa\aws-escwa.yaml
((Get-Content -path $PSScriptRoot\..\backend.yaml -Raw) -creplace "niybasicregistry.azurecr.io/bankdemo","$bankdemoRepositorySource") | Set-Content -Path $PSScriptRoot\..\aws-backend1.yaml
((Get-Content -path $PSScriptRoot\..\aws-backend1.yaml -Raw) -creplace "niybasicregistry.azurecr.io/es-metrics","$metricsRepositorySource") | Set-Content -Path $PSScriptRoot\..\aws-backend.yaml
((Get-Content -path $PSScriptRoot\..\openldap.yaml -Raw) -creplace "niybasicregistry.azurecr.io/openldap","$openldapRepositorySource") | Set-Content -Path $PSScriptRoot\..\aws-openldap.yaml
((Get-Content -path $PSScriptRoot\..\initdb.yaml -Raw) -creplace "niybasicregistry.azurecr.io/bankdemo","$bankdemoRepositorySource") | Set-Content -Path $PSScriptRoot\..\aws-initdb.yaml

& $PSScriptRoot\..\aws\aws-installimages.ps1 $PROJECTNAME user1

$serviceEndpoint = (kubectl get services ed60-bnkd-svc-mfdbfh-tn -o json | ConvertFrom-Json).status.loadBalancer.ingress[0].hostname

Write-Output "HostnameParameter
$serviceEndpoint" > $PSScriptRoot\..\..\VuGen\Scripts\ScriptParameters.dat

Get-Date
}