$PROJECTNAME=$args[0]
$GITBRANCH=$args[1]
function createShortcut{
    param(
        [Parameter(Mandatory = $True)]
        [string]
        $href,

        [Parameter(Mandatory = $True)]
        [string]
        $label
    )
    $wshShell = New-Object -ComObject "WScript.Shell"
    $urlShortcut = $wshShell.CreateShortcut(
      (Join-Path $wshShell.SpecialFolders.Item("AllUsersDesktop") "$label.lnk")
    )
    $urlShortcut.TargetPath = "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
    $urlShortcut.Arguments = $href
    $urlShortcut.Save()
}

$ROLE_ID=(curl http://169.254.169.254/latest/meta-data/iam/security-credentials).Content
$ROLE=(curl "http://169.254.169.254/latest/meta-data/iam/security-credentials/$ROLE_ID").Content | ConvertFrom-Json
aws configure set aws_access_key_id $($ROLE.AccessKeyId)
aws configure set aws_secret_access_key $($ROLE.SecretAccessKey)
aws configure set aws_session_token $($ROLE.Token)

$masterIp = aws ec2 describe-instances --filters Name=tag:LabInstanceType,Values=ContJenkins Name=tag:LabId,Values=$PROJECTNAME --query 'Reservations[*].Instances[*].PrivateIpAddress' --output text
if ($masterIp -eq "") {
    Write-Host "Failed to retrieve master instance ID"
    exit 1
}
$JenkinsUri="http://" + $masterIp + ":8081"
createShortcut -href $JenkinsUri -label "Jenkins"

aws eks update-kubeconfig --name $($PROJECTNAME.toLower())
Start-Process -FilePath "kubectl" -ArgumentList "proxy"
createShortcut -href "http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#!/login" -label "Kubernetes Dashboard"

Write-Output "Welcome to the Hands on Lab container demo environment.
Your git branch to use for development is $GITBRANCH.
Check the desktop for web shortcuts to jenkins and the kubernetes dashboard.
Below is the session token for logging in to the kubernetes dashboard:" > "C:\Users\Public\Desktop\Readme.txt"
Write-Output $(kubectl -n kube-system describe secret eks-admin-token-) >> "C:\Users\Public\Desktop\Readme.txt"
