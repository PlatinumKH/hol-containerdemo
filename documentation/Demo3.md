# Enterprise Developer in Kubernetes
# Creating an IMTK REST Web Service 
1. Open Eclipse
2. Open Bankdemo\COBOL Programs\cbl\BBANK70P.cbl in the editor
3. Select lines 224 to 236
    * Right click->Refactor To Section
    * Enter LOAN-CALCULATION in the edit box and press OK
4. Place the cursor within text LOAN-CALCULATION, press F3 to navigate to the section you created
5. Right click->Refactor Extract Section to program
    * Change the name to LOANCALC.cbl, press OK
6. Select the project, right click->New -> REST web service
    * Enter the name: LoanCalculator
    * Select Bankdemo\COBOL Programs\cbl\LOANCALC.cbl, press Finish
7. In LoanCalculator editor, LOANCALC Operation - Interface Fields pane 
    * Expand root\_in 
    * Select root\_in->BRE\_OUT\_OUTPUT\_STRUCTURE, press Delete
    * Expand root\_out 
    * Select root\_out->BRE\_INP\_INPUT\_STRUCTURE, press Delete 
8. Select the BankDemo->Web Services->LoanCalculator in the COBOL Explorer view
    * Right click->Properties, 
    * Switch to the Application Files tab
    * Set the Deployed application path to: /home/esadm/deploy/loadlib
    * Press OK
9. Select the BankDemo project in the COBOL Explorer view
    * Right click->Properties
    * Click on Micro Focus->Build Configurations->Link
    * Set Package services as COBOL archive (.car) files to Yes
    * Press OK
10. Edit Dockerfile
    * In the publish stage add the following with the other COPY statements (around line 84):
```COPY --from=BuildBankDemo /Projects/Eclipse/BankDemo/repos/**/*.car /home/esadm/deploy/packages/
```
    * Within the RUN block which imports the region definition (around line 101) add the following before the line starting mv:
```cd /home/esadm/deploy/packages && $COBDIR/bin/mfdepinst -s --server BANKDEMO --listener "Web Services and J2EE" LoanCalculator.car && \
```
11. Click Window->Show Perspective->Other->Git
    * Click on the Git Staging view select the following files:
    ** .cobolBuild - Projects/Eclipse/BankDemo
    ** .cobolProj - Projects/Eclipse/BankDemo
    ** BBANK70P.CBL - Sources/cbl
    ** LOANCALC.cbl - Sources/cbl
    ** LOANCALC.cbl.mfdirset - Projects/Eclipse/BankdDemo/.settings/Sources/cbl
    ** Dockerfile - System
    ** .inventory.so - Projects/Eclipse/BankDemo
    * Click the green + to stage the change
    * Enter a Commit message 
    * Press Commit and Push...
    